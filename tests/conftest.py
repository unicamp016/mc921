# pylint: disable=no-name-in-module, import-error, unused-argument
"""Test configuration that can be used across modules."""
import pytest

from exprerror import ERRORS
from lexer import UCLexer
from parser import UCParser


@pytest.fixture(name="uclexer")
def fixture_uclexer():
    """Create a uC lexer instance."""
    lexer = UCLexer()
    lexer.build()
    return lexer


@pytest.fixture(name="ucparser")
def fixture_ucparser():
    """Create a uC parser instance."""
    return UCParser()


def pytest_assertrepr_compare(config, op, left, right):
    """Define the assertion behavior for error code comparisons."""
    if isinstance(left, list) and isinstance(right, list) and op == "==":
        str_list = ["Comparing error codes:"]
        str_list.append("Expected errors:")
        for errcode in left:
            str_list.append(f"\tError {errcode}: {ERRORS[errcode]}")
        str_list.append("Output errors:")
        for errcode in right:
            str_list.append(f"\tError {errcode}: {ERRORS[errcode]}")
        return str_list
