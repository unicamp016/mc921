# pylint: disable=import-error, no-name-in-module
"""A test module for the uC code generator.

This module verifies that the generated code passes the assertion tests.
"""
import glob
import sys

import pytest

from exprcheck import ProgramCheckVisitor
from exprcode import UCCodeGen
from exprerror import (subscribe_errors, num_errors_reported, clear_errors,
                       SemanticError)
from interpreter import Interpreter


INPUT_FILES = sorted(glob.glob('assets/inputs/exprcode/*.uc'))
LLVM_INPUT_FILES = sorted(glob.glob('assets/inputs/llvm/*.uc'))
LLVM_OUTPUT_FILES = sorted(glob.glob('assets/outputs/llvm/*.out'))


def generate_ir(ucparser, input_code):
    """Generate the intermediate representation for the given code."""
    # Compute the abstract syntax tree from the source code.
    ast_root = ucparser.parse(input_code=input_code)

    # Clear error count and error code list.
    clear_errors()

    # Run the program checker and store reported errors.
    with subscribe_errors(lambda msg: sys.stdout.write(msg + '\n')):
        try:
            prog_check = ProgramCheckVisitor()
            prog_check.visit(ast_root)
            if num_errors_reported() == 0:
                code_gen = UCCodeGen()
                code_gen.visit(ast_root)
                return code_gen.code
        except SemanticError as exc:
            pytest.fail(exc, pytrace=True)


@pytest.mark.parametrize('input_file', INPUT_FILES)
def test_codegen_consistency(ucparser, capsys, input_file):
    """Test that the generated code doesn't fail any assertions."""
    input_code = open(input_file, 'r').read()
    gen_code = generate_ir(ucparser, input_code)

    interpreter = Interpreter()
    with pytest.raises(SystemExit):
        interpreter.run(gen_code)
    stdout, stderr = capsys.readouterr()

    assert stdout == ""
    assert stderr == ""


@pytest.mark.parametrize('files', zip(LLVM_INPUT_FILES, LLVM_OUTPUT_FILES))
def test_codegen_in_out(ucparser, capsys, files):
    """Test that the generated code's output matches the expected one."""
    input_file, output_file = files
    input_code = open(input_file, 'r').read()
    gen_code = generate_ir(ucparser, input_code)

    interpreter = Interpreter()
    with pytest.raises(SystemExit):
        interpreter.run(gen_code)
    stdout, _ = capsys.readouterr()
    expected_out = open(output_file, "r").read()
    assert stdout == expected_out
