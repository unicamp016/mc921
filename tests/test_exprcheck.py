# pylint: disable=import-error, no-name-in-module
"""A test module for the uC program checking visitor.

This module consists of unit tests that verify the error treatment of the
program checker.
"""
import glob
import sys

import pytest

from exprcheck import ProgramCheckVisitor
from exprerror import (subscribe_errors, errors_reported, num_errors_reported,
                       clear_errors, SemanticError)

INPUT_FILES_FAIL = sorted(glob.glob('assets/inputs/exprcheck/*.uc'))
INPUT_FILES_SUCC = sorted(glob.glob('assets/inputs/exprcode/*.uc'))


def check_program(ucparser, input_code):
    """Test the program checker for input files with known outputs."""
    # Compute the abstract syntax tree from the source code.
    ast_root = ucparser.parse(input_code=input_code)

    # Clear error count and error code list.
    clear_errors()

    # Run the program checker and store reported errors.
    with subscribe_errors(lambda msg: sys.stdout.write(msg + '\n')):
        try:
            prog_check = ProgramCheckVisitor()
            prog_check.visit(ast_root)
        except SemanticError:
            pass


@pytest.mark.parametrize('file_fail', INPUT_FILES_FAIL)
def test_file_failure(ucparser, file_fail):
    """Test that the program checker finds at least an error in these files."""
    input_code = open(file_fail, 'r').read()
    check_program(ucparser, input_code)
    assert num_errors_reported() > 0


@pytest.mark.parametrize('file_succ', INPUT_FILES_SUCC)
def test_file_success(ucparser, file_succ):
    """Test that the program checker doesn't find any errors in these files."""
    input_code = open(file_succ, 'r').read()
    check_program(ucparser, input_code)
    assert num_errors_reported() == 0


def test_missing_main_def(ucparser):
    """Check that an error is thrown if main isn't defined."""
    input_code = \
        """
        int a = 2;
        """
    check_program(ucparser, input_code)
    assert [1] == errors_reported()


def test_func_args_not_match(ucparser):
    """Verify that function argument mismatch error is thrown."""
    input_code = \
        """
        void fun(int a, int b, char c) {
            return;
        }

        int main() {
            int a, b, c;
            fun(a, b, c);
            return 0;
        }
        """
    check_program(ucparser, input_code)
    assert [3] == errors_reported()


def test_ref_non_array(ucparser):
    """Verify that referencing a non-array type throws an error."""
    input_code = \
        """
        int main() {
            int a, b;
            b = a[0];
            return 0;
        }
        """
    check_program(ucparser, input_code)
    assert [4] == errors_reported()


def test_array_reference_not_integer(ucparser):
    """Verify that array index references are integers."""
    input_code = \
        """
        int main() {
            char word[5];
            float i;
            for (i = 0.5; i < 5.0; i++){
                word[i] = 'a';
            }
            return 0;
        }
        """
    check_program(ucparser, input_code)
    assert [5] == errors_reported()


def test_if_expr_not_bool(ucparser):
    """Verify that an error if thrown if IF expression isn't a bool."""
    input_code = \
        """
        int main() {
            int x = 1;
            if (x) {
              x++;
            }
            return;
        }
        """
    check_program(ucparser, input_code)
    assert [6] == errors_reported()


def test_assert_expr_isbool(ucparser):
    """Verify that assert expression always evaluates to bool type."""
    input_code = \
        """
        int main() {
            int i, j;
            assert i + j;
            return 0;
        }
        """
    check_program(ucparser, input_code)
    assert [7] == errors_reported()


def test_return_type_mismatch(ucparser):
    """Check that an error is thrown if return types don't match."""
    input_code = \
        """
        int main() {
            return;
        }
        """
    check_program(ucparser, input_code)
    assert [8] == errors_reported()


def test_while_eval_bool(ucparser):
    """Verify that while expression must evaluate to bool."""
    input_code = \
        """
        int main() {
            int i;
            while (i){
                i++;
            }
            return;
        }
        """
    check_program(ucparser, input_code)
    assert [9] == errors_reported()


def test_for_expr_isbool(ucparser):
    """Verify that second for expression always evaluates to bool type."""
    input_code = \
        """
        int main() {
            int i, j;
            for (i; j; i++) {
                j++;
            }
            return 0;
        }
        """
    check_program(ucparser, input_code)
    assert [10] == errors_reported()


def test_symbol_conflict(ucparser):
    """Verify that symbol declaration conflicts are detected."""
    input_code = \
        """
        int main() {
            int a;
            char a;
            return 0;
        }
        """
    check_program(ucparser, input_code)
    assert [11] == errors_reported()


def test_symbol_different_scopes(ucparser):
    """Verify that declarations in different scopes don't conflict."""
    input_code = \
        """
        char a;
        int main() {
            int a;
            return 0;
        }
        """
    check_program(ucparser, input_code)
    assert num_errors_reported() == 0


def test_incompatible_types_declared(ucparser):
    """Verify type mismatch in symbol declaration."""
    input_code = \
        """
        int main() {
            int a = 3.2;
            return 0;
        }
        """
    check_program(ucparser, input_code)
    assert [12] == errors_reported()


def test_array_decl_not_integer(ucparser):
    """Verify that array declaration expression must be an integer."""
    input_code = \
        """
        void main() {
            char word[5.5];
            return;
        }
        """
    check_program(ucparser, input_code)
    assert [13] == errors_reported()


def test_list_elements_different(ucparser):
    """Verify that initializer list elements have the same type."""
    input_code = \
        """
        void main() {
            int numbers[2] = {0.5, "dog"};
            return;
        }
        """
    check_program(ucparser, input_code)
    assert [15] == errors_reported()


def test_binop_type_mismatch(ucparser):
    """Verify that binary operation expressions types must match."""
    input_code = \
        """
        void main() {
            int a = 2;
            float b = 3.14;

            int c = a + 3;    // OK
            int d = a + b;    // Error.  int + float

            return;
        }
        """
    check_program(ucparser, input_code)
    assert [16] == errors_reported()


def test_bin_op_not_supported(ucparser):
    """Check that an error is thrown if a binary operator isn't supported."""
    input_code = \
        """
        void main() {
            float a = 2.0, b = 3.0, c;
            c = a % b;
            return;
        }
        """
    check_program(ucparser, input_code)
    assert [17] == errors_reported()


def test_un_op_not_supported(ucparser):
    """Check that an error is thrown if a unary operator isn't supported."""
    input_code = \
        """
        void main() {
            int a;
            int c = *a;
            return;
        }
        """
    check_program(ucparser, input_code)
    assert [18] == errors_reported()


def test_assignment_expression_type_mismatch(ucparser):
    """Verify that assignment expression types must match."""
    input_code = \
        """
        int main() {
            char x = 'c';
            x = 3;
            return 0;
        }
        """
    check_program(ucparser, input_code)
    assert [19] == errors_reported()


def test_assignment_op_not_supported(ucparser):
    """Verify that an error is thrown if assignment op isn't supported."""
    input_code = \
        """
        int main() {
            char x = 'c';
            x += 'b';
            return 0;
        }
        """
    check_program(ucparser, input_code)
    assert [20] == errors_reported()


def test_missing_id_def(ucparser):
    """Check that an error is thrown if a symbol isn't defined."""
    input_code = \
        """
        int main() {
            int a = 3;
            int b = a + c;  // Error. 'c' not defined.
            return 0;
        }
        """
    check_program(ucparser, input_code)
    assert [21] == errors_reported()


def test_call_not_a_function(ucparser):
    """Check that an error is thrown if trying to call non-function symbol."""
    input_code = \
        """
        int main() {
            int a, arg;
            a(arg);
            return 0;
        }
        """
    check_program(ucparser, input_code)
    assert [22] == errors_reported()


def test_break_outside_loop(ucparser):
    """Verify that breaks defined outside fors and whiles throw an error."""
    input_code = \
        """
        int main() {
            int a = 3;
            break;
        }
        """
    check_program(ucparser, input_code)
    assert [23] == errors_reported()


def test_function_already_defined(ucparser):
    """Verify that you can't redefine a function."""
    input_code = \
        """
        int test(int a, int b) {
            return a + b;
        }
        int test(int a, int b) {
            return a - b;
        }
        int main() {
            return 0;
        }
        """
    check_program(ucparser, input_code)
    assert [24] == errors_reported()


def test_conflicting_function_declarations(ucparser):
    """Verify that an error is thrown for conflicting function declarations."""
    input_code = \
        """
        int test(int a, int b);
        int main() {
            return 0;
        }
        int test(int a, float b) {
            return a + (int) b;
        }
        """
    check_program(ucparser, input_code)
    assert [25] == errors_reported()


def test_conflicting_global_symbol_declarations(ucparser):
    """Verify that an error is thrown for conflicting symbol declarations."""
    input_code = \
        """
        int a = 3;
        float a;
        int main() {
            return 0;
        }
        """
    check_program(ucparser, input_code)
    assert [26] == errors_reported()


def test_nonconflicting_global_symbol_declarations(ucparser):
    """Verify that no error is thrown for multiple global declarations."""
    input_code = \
        """
        int a;
        int a = 3;
        int a;
        int main() {
            return 0;
        }
        """
    check_program(ucparser, input_code)
    assert num_errors_reported() == 0


def test_function_initialized_as_variable(ucparser):
    """Verify that an error is thrown if a function is initialized."""
    input_code = \
        """
        int foo(int a, int b) = 3;
        int main() {
            return 0;
        }
        """
    check_program(ucparser, input_code)
    assert [27] == errors_reported()


def test_global_symbol_redefined(ucparser):
    """Verify that an error is thrown if a global symbol is redefined."""
    input_code = \
        """
        int a = 3;
        int a = 2;
        int main() {
            return 0;
        }
        """
    check_program(ucparser, input_code)
    assert [28] == errors_reported()
