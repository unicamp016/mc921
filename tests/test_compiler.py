"""A test module for the uC compiler.

This module consists of integration tests that verify the behavior of the full
compiler.

Example:
    python -m pytest tests/
"""
import glob
import subprocess

import pytest


INPUT_FILES_OPT = sorted(glob.glob('assets/inputs/llvm/*.uc'))
OUTPUT_FILES_OPT = sorted(glob.glob('assets/outputs/llvm/*.out'))


@pytest.mark.parametrize('filenames', zip(INPUT_FILES_OPT, OUTPUT_FILES_OPT))
def test_compiler(filenames):
    """Test entire compiler for input files with known outputs."""
    input_fn, out_fn = filenames
    completed_proc = subprocess.run(
        ["./uc/uc.py", input_fn, "-s", "-l"],
        check=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    prog_out = completed_proc.stdout.decode("utf8")
    expected_out = open(out_fn, "r").read()
    assert prog_out == expected_out


@pytest.mark.parametrize('filenames', zip(INPUT_FILES_OPT, OUTPUT_FILES_OPT))
def test_compiler_with_opt(filenames):
    """Test entire compiler for input files with known outputs."""
    input_fn, out_fn = filenames
    completed_proc = subprocess.run(
        ["./uc/uc.py", input_fn, "-s", "-o", "-l"],
        check=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    prog_out = completed_proc.stdout.decode("utf8")
    expected_out = open(out_fn, "r").read()
    assert prog_out == expected_out
