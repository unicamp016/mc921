# pylint: disable=import-error, no-name-in-module
"""A test module for the LLVM code generator.

This module consists of unit tests that verify that the optimizer is able to
successfully translate both the original and optimized uC IR code to LLVM IR.
"""
import glob
import subprocess
import sys

import pytest

from dataflow import optimize_code
from exprcheck import ProgramCheckVisitor
from exprcode import UCCodeGen
from exprerror import (subscribe_errors, num_errors_reported, clear_errors,
                       SemanticError)
from interpreter import Interpreter

INPUT_FILES = sorted(glob.glob('assets/inputs/exprcode/*.uc'))

LLVM_INPUT_FILES = sorted(glob.glob('assets/inputs/llvm/*.uc'))
LLVM_OUTPUT_FILES = sorted(glob.glob('assets/outputs/llvm/*.out'))


def generate_ucir(ucparser, input_code, opt=False):
    """Return the orignal uC IR code."""
    # Compute the abstract syntax tree from the source code.
    ast_root = ucparser.parse(input_code=input_code)

    # Clear error count and error code list.
    clear_errors()

    # Run the program checker and store reported errors.
    with subscribe_errors(lambda msg: sys.stdout.write(msg + '\n')):
        try:
            prog_check = ProgramCheckVisitor()
            prog_check.visit(ast_root)
            if num_errors_reported() == 0:
                code_gen = UCCodeGen()
                code_gen.visit(ast_root)
                code = code_gen.code
                if opt:
                    code = optimize_code(code, llvm=True)
                return code
        except SemanticError as exc:
            pytest.fail(exc, pytrace=True)


@pytest.mark.parametrize('input_file', INPUT_FILES)
def test_llvm_consistency(ucparser, capsys, input_file):
    """Test that translating uC IR to LLVM doesn't break the code."""
    input_code = open(input_file, 'r').read()
    orig_code = generate_ucir(ucparser, input_code)

    interpreter = Interpreter()
    with pytest.raises(SystemExit):
        interpreter.run(orig_code)
    ucir_out, _ = capsys.readouterr()

    completed_proc = subprocess.run(
        ["python", "./uc/llvmcode.py", input_file],
        check=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    llvm_out = completed_proc.stdout.decode("utf8")

    assert llvm_out == ucir_out


@pytest.mark.parametrize('input_file', INPUT_FILES)
def test_llvm_opt_consistency(ucparser, capsys, input_file):
    """Test that translating optimized uC IR to LLVM doesn't break the code."""
    input_code = open(input_file, 'r').read()
    opt_code = generate_ucir(ucparser, input_code, opt=True)

    interpreter = Interpreter()
    with pytest.raises(SystemExit):
        interpreter.run(opt_code)
    ucir_opt_out, _ = capsys.readouterr()

    completed_proc = subprocess.run(
        ["python", "./uc/llvmcode.py", input_file],
        check=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    llvm_out = completed_proc.stdout.decode("utf8")

    assert llvm_out == ucir_opt_out


@pytest.mark.parametrize('files', zip(LLVM_INPUT_FILES, LLVM_OUTPUT_FILES))
def test_llvm_in_out(files):
    """Test translation of uC IR to LLVM with expected outputs."""
    input_fn, output_fn = files

    completed_proc = subprocess.run(
        ["python", "./uc/llvmcode.py", input_fn],
        check=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    llvm_out = completed_proc.stdout.decode("utf8")
    expected_out = open(output_fn, "r").read()
    assert llvm_out == expected_out


@pytest.mark.parametrize('files', zip(LLVM_INPUT_FILES, LLVM_OUTPUT_FILES))
def test_llvm_opt_in_out(files):
    """Test translation of optimized uC IR to LLVM with expected outputs."""
    input_fn, output_fn = files

    completed_proc = subprocess.run(
        ["python", "./uc/llvmcode.py", input_fn, "--opt"],
        check=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    llvm_out = completed_proc.stdout.decode("utf8")
    expected_out = open(output_fn, "r").read()
    assert llvm_out == expected_out
