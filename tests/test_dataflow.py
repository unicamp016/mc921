# pylint: disable=import-error, no-name-in-module
"""A test module for the uC dataflow optimizer.

This module consists of unit tests that verify that the optimizer is able to
achieve the minimum required speedup without breaking the code's functionality.
"""
import glob
import sys

import pytest

from dataflow import optimize_code
from exprcheck import ProgramCheckVisitor
from exprcode import UCCodeGen
from exprerror import (subscribe_errors, num_errors_reported, clear_errors,
                       SemanticError)
from interpreter import Interpreter

INPUT_FILES_OPT = sorted(glob.glob('assets/inputs/optimize/*.uc'))
OUTPUT_FILES_OPT = sorted(glob.glob('assets/outputs/optimize/*.out'))
INPUT_FILES_CODE = sorted(glob.glob('assets/inputs/exprcode/*.uc'))
LLVM_INPUT_FILES = sorted(glob.glob('assets/inputs/llvm/*.uc'))
LLVM_OUTPUT_FILES = sorted(glob.glob('assets/outputs/llvm/*.out'))


def generate_irs(ucparser, input_code):
    """Test the optimizer for input files with known outputs."""
    # Compute the abstract syntax tree from the source code.
    ast_root = ucparser.parse(input_code=input_code)

    # Clear error count and error code list.
    clear_errors()

    # Run the program checker and store reported errors.
    with subscribe_errors(lambda msg: sys.stdout.write(msg + '\n')):
        try:
            prog_check = ProgramCheckVisitor()
            prog_check.visit(ast_root)
            if num_errors_reported() == 0:
                code_gen = UCCodeGen()
                code_gen.visit(ast_root)
                opt_code = optimize_code(code_gen.code, False)
                return code_gen.code, opt_code
        except SemanticError as exc:
            pytest.fail(exc, pytrace=True)


def parse_opt_out(out_file):
    """Parse the output files for the expected speedup and instructions."""
    out_text = open(out_file, 'r').read()
    params_dict = {}
    for param in out_text.split(","):
        name, value = param.strip().split(" = ")
        params_dict[name] = float(value)
    return params_dict


@pytest.mark.parametrize('files', zip(INPUT_FILES_OPT, OUTPUT_FILES_OPT))
def test_opt_with_speedup(ucparser, capsys, files):
    """Test that optimizations don't break the code and speed it up."""
    input_file, out_file = files
    input_code = open(input_file, 'r').read()
    orig_code, opt_code = generate_irs(ucparser, input_code)

    interpreter = Interpreter()
    with pytest.raises(SystemExit):
        interpreter.run(orig_code)
    orig_out, orig_err = capsys.readouterr()

    interpreter = Interpreter()
    with pytest.raises(SystemExit):
        interpreter.run(opt_code)
    opt_out, opt_err = capsys.readouterr()

    assert opt_out == orig_out
    assert opt_err == orig_err

    params = parse_opt_out(out_file)
    assert len(opt_code) < params["otimizado"]


@pytest.mark.parametrize('input_file', INPUT_FILES_CODE)
def test_opt_consistency(ucparser, capsys, input_file):
    """Test that optimizations don't break the code."""
    input_code = open(input_file, 'r').read()
    orig_code, opt_code = generate_irs(ucparser, input_code)

    interpreter = Interpreter()
    with pytest.raises(SystemExit):
        interpreter.run(orig_code)
    orig_out, orig_err = capsys.readouterr()

    interpreter = Interpreter()
    with pytest.raises(SystemExit):
        interpreter.run(opt_code)
    opt_out, opt_err = capsys.readouterr()

    assert opt_out == orig_out
    assert opt_err == orig_err


@pytest.mark.parametrize('files', zip(LLVM_INPUT_FILES, LLVM_OUTPUT_FILES))
def test_opt_in_out(ucparser, capsys, files):
    """Test that optimizations output matches expected output."""
    input_file, output_file = files
    input_code = open(input_file, 'r').read()
    _, opt_code = generate_irs(ucparser, input_code)

    interpreter = Interpreter()
    with pytest.raises(SystemExit):
        interpreter.run(opt_code)
    opt_out, _ = capsys.readouterr()

    expected_out = open(output_file, "r").read()
    assert opt_out == expected_out
