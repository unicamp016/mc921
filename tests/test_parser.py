# pylint: disable=no-name-in-module
"""A test module for the uC language parser.

This module tests the output of the parser for all predefined inputs along with
verifying specific functionality through simple unit tests.

Example:
    python -m pytest tests/
"""
import glob
import sys

import pytest


INPUT_FILES = sorted(glob.glob('assets/inputs/parser/*.uc'))
OUTPUT_FILES = sorted(glob.glob('assets/outputs/parser/*.ast'))


@pytest.mark.parametrize('files', zip(INPUT_FILES, OUTPUT_FILES))
def test_file(ucparser, capsys, files):
    """Test the parser for input files with known outputs."""
    source_program, output = files

    source_code = open(source_program, 'r').read()
    ast_root = ucparser.parse(input_code=source_code)
    ast_root.show(buf=sys.stdout, showcoord=True)

    # Capture the parser's output
    captured = capsys.readouterr()
    # Read the expected output
    expected_output = open(output, 'r').read()

    assert captured.out == expected_output
