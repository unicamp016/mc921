# pylint: disable=import-error
"""A test module for the uC language lexer.

This module tests the output of the lexer for all predefined inputs along with
verifying specific functionality through simple unit tests.

Example:
    python -m pytest tests/
"""
import glob

import pytest

INPUT_FILES = sorted(glob.glob('assets/inputs/lexer/*.uc'))
OUTPUT_FILES = sorted(glob.glob('assets/outputs/lexer/*.out'))


@pytest.mark.parametrize('files', zip(INPUT_FILES, OUTPUT_FILES))
def test_file(uclexer, capsys, files):
    """Test the lexer for input files with known outputs."""
    source_program, output = files

    # Run the lexer
    uclexer.scan(source_program)

    # Capture the lexer's output
    captured = capsys.readouterr()
    # Read the expected output
    expected_output = open(output, 'r').read()

    assert captured.out == expected_output
