.PHONY: test test_lexer test_parser test_exprcheck test_exprcode \
				test_compiler lint clean

PYTHON_FILES = $(wildcard tests/*.py uc/*.py)
export PYTHONPATH := $(shell pwd)/uc

test:
	python -m pytest tests/ -vv

test_lexer:
	python -m pytest tests/$@.py -vv

test_parser:
	python -m pytest tests/$@.py -vv

test_exprcheck:
	python -m pytest tests/$@.py -vv

test_exprcode:
	python -m pytest tests/$@.py -vv

test_dataflow:
	python -m pytest tests/$@.py -vv

test_llvmcode:
	python -m pytest tests/$@.py -vv

test_compiler:
	python -m pytest tests/$@.py -vv

lint:
	@echo "\n------------------PYLINT------------------\n"
	pylint $(PYTHON_FILES) --disable=C --disable=R
	@echo "\n------------------PYCODESTYLE------------------\n"
	pycodestyle $(PYTHON_FILES)
	@echo "\n------------------PYDOCSTYLE------------------\n"
	pydocstyle $(PYTHON_FILES)

clean:
	@rm -r bin/*
