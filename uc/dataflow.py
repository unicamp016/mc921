"""Dataflow analysis module.

This module is responsible for building the CFG for a given function definition
and performing the reaching definitions analysis on top of it for future
optimizations.
"""
import abc
import argparse
import collections
import functools
import sys

import graphviz

from exprcheck import ProgramCheckVisitor
from exprcode import UCCodeGen, format_instruction
from exprerror import num_errors_reported, subscribe_errors, SemanticError
from interpreter import Interpreter
from optimize import ConstantProp, DeadCodeElim
from parser import UCParser


def bfs(root, func, *args, **kwargs):
    """Traverse the graph in a breadth-first manner.

    Args:
        root: The root node to start the graph traversal, the only requirement
              is that the root node must have a 'successors' attribute that
              evaluates to an iterable with the nodes that the current node
              points to.
        func: The function that should be executed on each node once it's
              visited.
        *args: Any positional arguments that should be passed to func.
        **kwargs: Any keyword arguments that should be passed to func.
    """
    visited = set()
    node_queue = collections.deque([root])
    while node_queue:
        node = node_queue.popleft()
        func(node, *args, **kwargs)
        for suc_node in node.successors:
            if suc_node not in visited:
                visited.add(suc_node)
                node_queue.append(suc_node)


class Block:
    """Block class definition used as a building block for the CFG."""

    def __init__(self, label):
        """Initialize a generic block object.

        Attributes:
            label: The label that identifies the block.
            instructions: A list of tuples with the instructions for the block.
            predecessors: List of all blocks that precede this one in the CFG.
            successors: List of all blocks that succede this on in the CFG.
        """
        self.label = label
        self.instructions = []
        self.predecessors = []
        self.successors = []

    def append(self, inst):
        """Append the given instruction to the block's instruction list."""
        self.instructions.append(inst)

    def __iter__(self):
        """Iterate through the block's instruction set."""
        return iter(self.instructions)

    def __str__(self):
        """Return the string representation for the block.

        This representation showcases the entire graph component that's
        reachable from this block. A newline is used for every node in the
        graph and the arrow points to the labels of the successor blocks.
        """
        adj_list = {}
        bfs(self, lambda b, adj: adj.update(
            {b.label: [s.label for s in b.successors]}
        ), adj_list)
        ret_str = ""
        for label, sucs in adj_list.items():
            ret_str += f"{label} -> {sucs}\n"
        return ret_str

    def __repr__(self):
        """Return the block's label."""
        return self.label


@functools.lru_cache(maxsize=None)
def fetch_block(label):
    """Fetch or create a block object for the given label."""
    return Block(label)


class CFG:
    """Control flow graph object for dataflow analysis."""

    def __init__(self, code, output_dir=None):
        """Initialize the CFG with the instructions for the func def.

        Attributes:
            root_block: The root block of the CFG, from where all other blocks
                are reachable.
            fname: The filename (including any directories) to use for saving
                any artifacts from graphviz.
            print_order: The list of blocks in the order in which they need
                to be listed when generating the code.
            graph: A Digraph object used to visualize the CFG.
        """
        self.root_block, self.print_order = self._create_cfg(code)
        self.output_dir = output_dir
        self.graph = None

        # Create a list with all of the blocks
        self.blocks = []
        bfs(self.root_block, self.blocks.append)

        # Create a dictionary that maps block labels to instruction lists
        # for future lookup by the dataflow analysis classes.
        self.inst_dict = {}
        bfs(
            self.root_block,
            lambda b: self.inst_dict.update({b.label: b.instructions})
        )

    def _create_cfg(self, code):
        """Generate a CFG for the given set of instructions.

        Args:
            code: List of tuples with all the instructions for a function
                definition.
        Returns:
            The root Block object for the function definition's CFG.
        """
        fetch_block.cache_clear()

        # Get the function name
        func_name = code[0][1][1:]

        # Create a list of blocks in the order in which they should be printed.
        print_order = []

        # Initialize the CFG with an empty function block that points to the
        # entry block.
        func_block = fetch_block(func_name)
        cur_block = fetch_block("%entry")
        func_block.successors.append(cur_block)
        cur_block.predecessors.append(func_block)
        print_order.append(cur_block)

        for inst in code:
            opname = inst[0]
            if len(inst) == 1 and opname != "return_void":
                next_label = f"%{opname[:-1]}"
                next_block = fetch_block(next_label)
                if cur_block is not None:
                    cur_block.successors.append(next_block)
                    next_block.predecessors.append(cur_block)
                cur_block = next_block
                print_order.append(cur_block)
            elif cur_block is not None:
                if opname == "jump":
                    jump_label = inst[1]
                    new_block = fetch_block(jump_label)
                    cur_block.successors.append(new_block)
                    new_block.predecessors.append(cur_block)
                    cur_block.append(inst)
                    cur_block = None
                elif opname == "cbranch":
                    true_label = inst[2]
                    false_label = inst[3]
                    true_block = fetch_block(true_label)
                    false_block = fetch_block(false_label)
                    cur_block.successors.extend([true_block, false_block])
                    true_block.predecessors.append(cur_block)
                    false_block.predecessors.append(cur_block)
                    cur_block.append(inst)
                    cur_block = None
            if cur_block is not None:
                cur_block.append(inst)

        print_order = self._remove_dangling_blocks(print_order)
        print_order = self._collapse_blocks(print_order)
        print_order = self._remove_singleton_blocks(print_order)
        return func_block, print_order

    def _remove_singleton_blocks(self, print_order):
        """Remove blocks with a single jump instruction."""
        for blk in print_order:
            if len(blk.instructions) == 2 and \
               blk.instructions[-1][0] == "jump":
                # Propagate the jump to the previous block.
                jump_target = blk.instructions[-1][1]
                for pred in blk.predecessors:
                    last_inst = pred.instructions[-1]
                    new_inst = tuple(
                        t.replace(blk.label, jump_target) for t in last_inst)
                    pred.instructions[-1] = new_inst
                    pred.successors.remove(blk)
                    pred.successors.extend(blk.successors)
                for suc in blk.successors:
                    suc.predecessors.remove(blk)
                    for pred in blk.predecessors:
                        if pred not in suc.predecessors:
                            suc.predecessors.append(pred)
                print_order.remove(blk)
        return print_order

    def _collapse_blocks(self, print_order):
        """Collapse blocks that always succede one another."""
        collapse_set = set(print_order)
        while collapse_set:
            blk = collapse_set.pop()
            if len(blk.successors) == 1 and \
               len(blk.successors[0].predecessors) == 1:
                suc = blk.successors[0]
                blk.instructions = blk.instructions[:-1]
                suc.instructions = suc.instructions[1:]
                blk.instructions.extend(suc.instructions)

                # Update successors and predecessors
                blk.successors = suc.successors
                for blk_suc in blk.successors:
                    blk_suc.predecessors.remove(suc)
                    blk_suc.predecessors.append(blk)

                if suc in collapse_set:
                    collapse_set.remove(suc)
                collapse_set.add(blk)
                print_order.remove(suc)
        return print_order

    def _remove_dangling_blocks(self, print_order):
        """Remove unreachable blocks."""
        for blk in print_order:
            if len(blk.predecessors) == 0:
                for suc in blk.successors:
                    suc.predecessors.remove(blk)
                print_order.remove(blk)
        return print_order

    def _draw_cfg(self, block):
        assert len(block.successors) <= 2
        _name = block.label
        if len(block.predecessors) == 0:
            self.graph.node(
                _name,
                label=None,
                _attributes={'shape': 'ellipse'}
            )
            self.graph.edge(_name, block.successors[0].label)
        elif len(block.successors) == 2:
            _label = "{" + _name + ":\\l\t"
            for _inst in block.instructions[1:]:
                _label += format_instruction(_inst) + "\\l\t"
            _label += "|{<f0>T|<f1>F}}"
            self.graph.node(_name, label=_label)
            self.graph.edge(_name + ":f0", block.successors[0].label)
            self.graph.edge(_name + ":f1", block.successors[1].label)
        else:
            _label = "{" + _name + ":\\l\t"
            for _inst in block.instructions[1:]:
                _label += format_instruction(_inst) + "\\l\t"
            _label += "}"
            self.graph.node(_name, label=_label)
            if block.successors:
                self.graph.edge(_name, block.successors[0].label)

    def view(self, name_suffix="", show=False):
        """Draw the CFG starting from the root node."""
        self.graph = graphviz.Digraph(
            name=self.root_block.label + name_suffix,
            node_attr={"shape": "record"}
        )
        bfs(self.root_block, self._draw_cfg)
        self.graph.render(
            directory=self.output_dir,
            cleanup=True,
            view=show,
            format="pdf"
        )

    def gen_code(self):
        """Generate the code as a list of insts from the CFG."""
        insts = []
        for block in self.print_order:
            insts.extend(block.instructions)
        return insts

    def recompute(self):
        """Generate the code from the CFG and recompute it.

        This is helpful to remove unreachable code from the CFG after running
        an optimization.
        """
        code = self.gen_code()
        self.__init__(code, self.output_dir)


class DataflowAnalysis(abc.ABC):
    """Abstract class for different types of dataflow analysis."""

    def __init__(self, cfg):
        """Initialize the dataflow analysis object.

        Attributes:
            cfg: The control flow graph used to perform the analysis.
            defns: Dictionary with all of the local variable definitions (read
                and store) made throughout the function. Maps variable register
                to a list of tuples of the form (block_label, pos) where pos
                is the index of the instruction in the block referred by
                block_label where the definition was made.
            gen: Dictionary mapping block object to a set of all definitions (
                i.e. block and pos tuples) generated by the block.
            kill: Dictionary mapping block object to a set of all definitions (
                i.e. block and pos tuples) killed by the block.
            ins: Dictionary mapping block object to a set of all definitions (
                i.e. block and pos tuples) that are inputs to the block.
            outs: Dictionary mapping block object to a set of all definitions (
                i.e. block and pos tuples) that are output by the block.
        """
        self.cfg = cfg
        self.defns = {}
        self.gen = {}
        self.kill = {}
        self.ins = {}
        self.outs = {}

    def analyze(self):
        """Run the analysis to compute the in and out set."""
        self.compute_gen_kill()
        self.compute_in_out()

    @abc.abstractmethod
    def compute_gen_kill(self):
        """Compute the gen and kill sets for each block."""
        raise NotImplementedError

    @abc.abstractmethod
    def compute_in_out(self):
        """Compute the in and out sets for each block."""
        raise NotImplementedError


class Liveness(DataflowAnalysis):
    """Performs the liveness analysis of a given CFG."""

    def __init__(self, cfg, global_vars):
        """Initialize liveness analysis with CFG and set of global variables.

        global_vars: Set of global variable names used as initial value for
            the out set of the exit block.
        """
        super().__init__(cfg)
        self.global_vars = global_vars

    def compute_in_out(self):
        """Compute the in and out sets for liveness analysis."""
        # Find the exit block of the function
        exit_block = None
        for blk in self.cfg.blocks:
            self.ins[blk] = set()
            if len(blk.successors) == 0:
                exit_block = blk

        self.outs[exit_block] = self.global_vars
        self.ins[exit_block] = \
            self.gen[exit_block].union(self.outs[exit_block])

        changed = set(self.cfg.blocks) - {exit_block}
        while changed:
            blk = changed.pop()
            self.outs[blk] = set()
            for suc in blk.successors:
                self.outs[blk] = self.outs[blk].union(self.ins[suc])
            old_in = self.ins[blk]
            self.ins[blk] = \
                self.gen[blk].union(self.outs[blk] - self.kill[blk])
            if self.ins[blk] != old_in:
                for pred in blk.predecessors:
                    changed.add(pred)

    def compute_gen_kill(self):
        """Compute the gen and kill sets for the liveness analysis."""
        for block in self.cfg.blocks:
            self.gen[block] = set()
            self.kill[block] = set()

        for block in self.cfg.blocks:
            for inst in block.instructions:
                opname = inst[0].split("_")[0]
                is_ptr = inst[0].endswith("*")
                if not is_ptr:
                    if opname in ("store", "read"):
                        self.kill[block].add(inst[-1])
                    elif opname in ("load", "elem"):
                        if inst[1] not in self.kill[block]:
                            self.gen[block].add(inst[1])


class ReachingDefinitions(DataflowAnalysis):
    """This class performs the reaching definitions analysis given a CFG."""

    def __init__(self, cfg, global_insts, global_defs, param_names):
        """Initialize reaching definitions analysis with CFG and globals.

        Args:
            global_insts: Dictionary mapping global variable names to the
                instruction that defines them.
            global_defs: Dictionary mapping global variable names to the
                functions in the program that modify it.
            param_names: List of parameter names.
        """
        super().__init__(cfg)
        self._add_global_defs(global_insts, global_defs)
        self._add_params(param_names)

    def _add_global_defs(self, global_insts, global_defs):
        """Add the global definitions to the root block of the CFG.

        This enables constant propagation optimizations to consider the
        available constant global variables.
        """
        for gdef, funcs in global_defs.items():
            ginst = global_insts[gdef]
            if len(funcs) == 0:
                self.cfg.root_block.append(ginst)
            else:
                # Create an instruction that evaluates the global variable
                # as NAC. The type of the instruction doesn't matter.
                nac_inst = ("read_int", ginst[1])
                self.cfg.root_block.append(nac_inst)

    def _add_params(self, param_names):
        """Add the function parameters as NACs by appending read insts."""
        for pname in param_names:
            self.cfg.root_block.instructions.append(
                ("read_int", pname)
            )

    def compute_in_out(self):
        """Compute the in and out sets for reaching definitions analysis."""
        for blk in self.cfg.blocks:
            self.outs[blk] = set()
        changed = set(self.cfg.blocks)
        while changed:
            blk = changed.pop()
            self.ins[blk] = set()
            for pred in blk.predecessors:
                self.ins[blk] = self.ins[blk].union(self.outs[pred])
            old_out = self.outs[blk]
            self.outs[blk] = \
                self.gen[blk].union(self.ins[blk] - self.kill[blk])
            if self.outs[blk] != old_out:
                for suc in blk.successors:
                    changed.add(suc)

    def compute_gen_kill(self):
        """Compute the gen and kill sets for reaching definitions analysis."""
        # Compute gen sets for each of the blocks while keeping track of all
        # of the definitions for each target throughout the code.
        for block in self.cfg.blocks:
            self.gen[block] = self._get_definitions(block)

        # Compute the kill set for each block.
        for block, defns in self.gen.items():
            self.kill[block] = set()
            for target, defn in defns.items():
                def_kill_set = self.defns[target] - {defn}
                self.kill[block] = self.kill[block].union(def_kill_set)

        # Transform the gen dictionary mapping targets to def instructions into
        # a set of def instructions.
        for block, defns in self.gen.items():
            self.gen[block] = set()
            for _, defn in defns.items():
                self.gen[block].add(defn)

    def _get_definitions(self, block):
        """Find and return targets that where defined in the given block."""
        defs = dict()
        for pos, inst in enumerate(block.instructions):
            opname = inst[0].split("_")[0]

            if opname in ("store", "read"):
                target = inst[-1]
            elif opname == "global":
                target = inst[1]
            else:
                continue
            defs[target] = (block.label, pos)
            # Add instruction to the global dictionary of definitions
            if self.defns.get(target) is None:
                self.defns[target] = set()
            self.defns[target].add((block.label, pos))

        return defs


def split_instructions(code):
    """Break the entire code into separate instruction sets.

    Args:
        code: The list of tuples with all instructions for the input uC code.
    Returns:
        func_defs: List of lists of tuples with the instructions pertaining to
            each function definition.
        global_insts: Map between global variable names and the instruction
            that allocates it.
        global_defs: Map between variable names (e.g. @x) and a set of
            all functions where the global variable was modified.
    """
    func_defs = []
    func_name = None
    func_names = set()
    global_insts = {}
    global_vars = set()
    global_defs = {}
    for inst in code:
        opname = inst[0].split("_")[0]
        if opname == "global":
            global_insts[inst[1]] = inst
            global_vars.add(inst[1])
        elif opname == "define":
            func_defs.append([inst])
            func_name = inst[-1]
            func_names.add(func_name)
        else:
            func_defs[-1].append(inst)
            if opname == "store" and inst[-1] in global_vars:
                modified_vars = global_defs.get(inst[-1], set())
                modified_vars.add(func_name)
                global_defs[inst[-1]] = modified_vars

    for gvar in global_vars:
        if gvar not in global_defs:
            global_defs[gvar] = set()
    return func_defs, global_insts, global_defs


def run_opts(code, global_insts, global_defs, param_names, iteration,
             replace_store, show_cfg, debug):
    """Run one iteration with both dead code and const prop optimizations."""
    cfg = CFG(
        code=code,
        output_dir="bin",
    )
    reach_def = ReachingDefinitions(
        cfg,
        global_insts,
        global_defs,
        param_names
    )
    reach_def.analyze()
    const_prop = ConstantProp(reach_def, replace_store=replace_store)
    const_prop.optimize()
    cfg.recompute()
    if debug:
        cfg.view(f".cprop.{iteration}", show_cfg)
    liveness = Liveness(
        cfg,
        global_vars=set(global_defs.keys())
    )
    liveness.analyze()
    deadcode = DeadCodeElim(
        liveness,
        set(global_defs.keys())
    )
    deadcode.optimize()
    cfg.recompute()
    if debug:
        cfg.view(f".deadc.{iteration}", show_cfg)
    new_code = cfg.gen_code()
    return new_code


def remove_insts(code, remove_pos):
    """Remove instructions in the given set of indices."""
    new_code = []
    for pos, inst in enumerate(code):
        if pos not in remove_pos:
            new_code.append(inst)
    return new_code


def remove_allocs(code, scope=None):
    """Remove any allocs that aren't used throughout the program."""
    rm_allocs = {}
    for pos, inst in enumerate(code):
        opname = inst[0].split("_")[0]
        if opname == "global" and scope == "global":
            rm_allocs[inst[1]] = pos
        elif opname == "alloc" and scope == "local":
            rm_allocs[inst[-1]] = pos
        else:
            for token in inst:
                if isinstance(token, collections.abc.Hashable) and \
                   token in rm_allocs:
                    rm_allocs.pop(token)
    return remove_insts(code, set(rm_allocs.values()))


def remove_dummy_jumps(code):
    """Remove jumps that are immediately followed by the label they jump to."""
    remove_pos = set()
    for pos, (prev_inst, next_inst) in enumerate(zip(code[:-1], code[1:])):
        prev_opname = prev_inst[0].split("_")[0]
        if prev_opname == "jump" and next_inst[0].endswith(":"):
            prev_target = prev_inst[-1][1:]
            if prev_target == next_inst[0][:-1]:
                remove_pos.add(pos)
    return remove_insts(code, remove_pos)


def optimize_func(func_def, global_insts, global_defs, param_names, show_cfg,
                  debug):
    """Optimize the given function by iterating opts until convergence."""
    # Save initial code to compute speedup
    init_code = func_def

    cur_code = None
    new_code = init_code
    iteration = 1
    while cur_code != new_code:
        cur_code = new_code
        new_code = run_opts(cur_code,
                            global_insts,
                            global_defs,
                            param_names,
                            iteration,
                            replace_store=False,
                            show_cfg=show_cfg,
                            debug=debug)
        new_code = remove_allocs(new_code, scope="local")
        iteration += 1
    new_code = run_opts(cur_code, global_insts, global_defs, param_names,
                        iteration, True, show_cfg, debug)
    return new_code


def extract_params(code):
    """Extract the parameter names for the given function definition."""
    param_names = []
    for inst in code:
        if inst[0].startswith("define"):
            param_names = [ptok[1] for ptok in inst[2:]]
            break
    return param_names


def optimize_code(code, show_cfg=False, debug=False, llvm=False):
    """Optimize the entire code by optimizing funcs and applying final opts."""
    func_defs, global_insts, global_defs = split_instructions(code)
    opt_code = []
    for func_def in func_defs:
        param_names = extract_params(func_def)
        func_def = optimize_func(
            func_def, global_insts, global_defs, param_names, show_cfg, debug)
        opt_code.extend(func_def)

    # Append globals and remove any unused ones
    global_insts = list(global_insts.values())
    opt_code = global_insts + opt_code
    opt_code = remove_allocs(opt_code, scope="global")
    if not llvm:
        opt_code = remove_dummy_jumps(opt_code)
    return opt_code


def main():
    """Parse input stream and run the program."""
    parser = argparse.ArgumentParser(description="Run the uC interpreter.")
    parser.add_argument(
        "filename", type=str, nargs="?", default=None,
        help="The filename with the uC code that should be checked. In case "
             "no argument filename is provided, stdin is used by default.")
    parser.add_argument(
        "-s", "--show_cfg", action="store_true",
        help="Whether or not to show the CFG drawn with graphviz using the"
             "default application to display PDF files.")
    parser.add_argument(
        "--llvm", action="store_true",
        help="If true, optimizations don't remove dummy jumps from code, "
             "those are needed for LLVM translation.")
    parser.add_argument(
        "-r", "--run", action="store_true",
        help="If true, run the optimized code through the interpreter.")
    parser.add_argument(
        "-g", "--debug", action="store_true",
        help="Whether or not to run the dataflow optimizer in debugging mode. "
             "If true, the optimizer outputs the pdfs for the intermediate "
             "runs of the optimizations.")
    args = parser.parse_args()

    if args.filename:
        file = open(args.filename, 'r')
    else:
        file = sys.stdin

    ucparser = UCParser()
    ast_root = ucparser.parse(input_code=file.read())
    if ucparser.nerrors == 0:
        with subscribe_errors(lambda msg: sys.stdout.write(msg + '\n')):
            try:
                prog_check = ProgramCheckVisitor()
                prog_check.visit(ast_root)
                if num_errors_reported() == 0:
                    code_gen = UCCodeGen()
                    code_gen.visit(ast_root)
                    opt_code = optimize_code(code_gen.code, args.show_cfg,
                                             args.debug, args.llvm)
                    print(f"Original: {len(code_gen.code)}\n"
                          f"Optimized: {len(opt_code)}\n"
                          f"Speedup: {len(code_gen.code)/len(opt_code)}")
                    if args.debug:
                        for inst in opt_code:
                            print(format_instruction(inst))
                    if args.run:
                        interpreter = Interpreter()
                        interpreter.run(opt_code)
            except SemanticError:
                pass


if __name__ == "__main__":
    main()
