# pylint: disable=global-statement
"""Program checking errors module.

This module has dictionary mapping error codes to a message describing the
type of error. This is mainly used to keep track of what errors are being
thrown and create tests that check exactly what errors are thrown by a given
program. The difference between these messages and the ones in the exprcheck
file is that these don't depend on any parameters and are generic error
messages, while the others depend on a given program instance.
"""
from contextlib import contextmanager

_SUBSCRIBERS = []
_NUM_ERRORS = 0
_ERRCODES = []


class SemanticError(Exception):
    """Semantic error class raised by the program checker."""


def error(coord, errcode, message, filename=None):
    """Report a compiler error to all subscribers."""
    global _NUM_ERRORS, _ERRCODES
    if not filename:
        errmsg = "{}:{}: {}".format(coord.line, coord.column, message)
    else:
        errmsg = "{}:{}:{}: {}".format(filename, coord.line, coord.column,
                                       message)
    for subscriber in _SUBSCRIBERS:
        subscriber(errmsg)
    _ERRCODES.append(errcode)
    _NUM_ERRORS += 1

    # We want to exit on the first encountered error.
    raise SemanticError


def errors_reported():
    """Return the list of error codes reported."""
    return _ERRCODES


def num_errors_reported():
    """Return number of errors reported."""
    return _NUM_ERRORS


def clear_errors():
    """Clear the total number of errors reported."""
    global _NUM_ERRORS, _ERRCODES
    _NUM_ERRORS = 0
    _ERRCODES = []


@contextmanager
def subscribe_errors(handler):
    """Context manager that allows monitoring of compiler error messages.

    Use as follows where handler is a callable taking a single argument
    which is the error message string:

    with subscribe_errors(handler):
        ... do compiler ops ...
    """
    _SUBSCRIBERS.append(handler)
    try:
        yield
    finally:
        _SUBSCRIBERS.remove(handler)


ERRORS = {
    1: "No function declaration main in the program.",
    2: "Unable to define nested function.",
    3: "Function arguments don't match parameters.",
    4: "Attempting to reference non-array type.",
    5: "Array reference expression must be an integer.",
    6: "If statement expression must evaluate to bool.",
    7: "Assert expression must evaluate to bool.",
    8: "Return statement type doesn't match function return type.",
    9: "While statement expression must evaluate to bool.",
    10: "Second for expression must evaluate to bool.",
    11: "Symbol has already been declared in the same scope.",
    12: "Incompatible types in symbol declaration.",
    13: "Array declaration constant expression must evaluate to integer.",
    15: "Initializer list elements must all have the same type.",
    16: "Binary operation type mismatch.",
    17: "Binary operator not supported.",
    18: "Unary operator not supported.",
    19: "Type mismatch in expression assignment.",
    20: "Assignment op not supported.",
    21: "No identifier was declared for the given symbol.",
    22: "Tried to call non-function symbol.",
    23: "Break statement should be inside a for or while loop.",
    24: "Function has already been defined.",
    25: "Conflicting types in function declaration.",
    26: "Conflicting types in global symbol declaration.",
    27: "Function is initialized like a variable.",
    28: "Global symbol has already been defined.",
}
