"""Defines the types used for semantic checking in the uC language."""


class uCType(object):
    """Class that represents a type in the uC language.

    Types are declared as singleton instances of this type.
    """

    def __init__(self, name, default=None, unary_ops=None, binary_ops=None,
                 rel_ops=None, assign_ops=None):
        """Initialize generic uC type with supported ops."""
        self._typename = name
        self._opname = name
        self.unary_ops = unary_ops or set()
        self.binary_ops = binary_ops or set()
        self.rel_ops = rel_ops or set()
        self.assign_ops = assign_ops or set()
        self.default = default

    @property
    def typename(self):
        """Return typename for generic uCType."""
        return self._typename

    @property
    def opname(self):
        """Return the operation code name for a generic UCType."""
        return self._opname


IntType = uCType(
    "int",
    unary_ops={"-", "+", "--", "++", "p--", "p++", "&"},
    binary_ops={"+", "-", "*", "/", "%"},
    rel_ops={"==", "!=", "<", ">", "<=", ">="},
    assign_ops={"=", "+=", "-=", "*=", "/=", "%="},
    default=0
)

FloatType = uCType(
    "float",
    unary_ops={"-", "+", "--", "++", "p--", "p++", "&"},
    binary_ops={"+", "-", "*", "/"},
    rel_ops={"==", "!=", "<", ">", "<=", ">="},
    assign_ops={"=", "+=", "-=", "*=", "/="}
)

CharType = uCType(
    "char",
    unary_ops={"&"},
    rel_ops={"==", "!=", "&&", "||"},
    assign_ops={"="}
)

BoolType = uCType(
    "bool",
    unary_ops={"!", "&"},
    rel_ops={"==", "!=", "&&", "||"},
    assign_ops={"="}
)

StringType = uCType(
    "string",
    rel_ops={"==", "!="}
)

VoidType = uCType(
    "void",
    unary_ops={"&"},
    rel_ops={"==", "!="}
)


class PtrType(uCType):
    """Pointer type class needs to be instantiated for each declaration."""

    def __init__(self, ptr_type):
        """Initialize array type.

        Args:
            ptr_type: Any of the uCTypes can be used as the pointer type. This
                means that there's support for nested pointers.
        """
        self.type = ptr_type
        super().__init__(None, unary_ops={"*", "&"}, rel_ops={"==", "!="},
                         assign_ops={"="})

    @property
    def typename(self):
        """Return the typename of the pointer type."""
        self._typename = f"{self.type.typename}_ptr"
        return self._typename

    @property
    def opname(self):
        """Return the typename as expected by the code generator."""
        self._opname = f"{self.type.opname}_*"
        return self._opname

    def __eq__(self, other):
        """Verify equality between two pointer types."""
        if isinstance(other, (PtrType, ArrayType)) and self.type == other.type:
            return True
        if other is StringType and self.type is CharType:
            return True
        return False


class ArrayType(uCType):
    """Array type class needs to be instantianted for each declaration."""

    def __init__(self, arr_type, size=None):
        """Initialize array type.

        Args:
            type: Any of the uCTypes can be used as the array's type. This
                means that there's support for nested types, like matrices.
            size: Integer with the length of the array.
        """
        self.type = arr_type
        self.size = size
        self._total_size = None
        super().__init__(None, unary_ops={"*", "&"}, rel_ops={"==", "!="})

    @property
    def typename(self):
        """Return the typename of the array type."""
        if self.size is None:
            self._typename = f"array[{self.type.typename}]"
        else:
            self._typename = f"array[{self.type.typename}({self.size})]"
        return self._typename

    @property
    def opname(self):
        """Return the typename as expected by the opcode."""
        if self.size is None:
            self._opname = f"{self.type.opname}_*"
        else:
            dims = [self.size]
            start = self
            while isinstance(start.type, ArrayType):
                start = start.type
                dims.append(start.size)
            self._opname = f"{start.type.opname}"
            for dim in dims:
                self._opname += f"_{dim}"
        return self._opname

    @property
    def total_size(self):
        """Return the total size of an array type."""
        if isinstance(self.type, ArrayType):
            self._total_size = self.size * self.type.total_size
        else:
            self._total_size = self.size
        return self._total_size

    def __eq__(self, other):
        """Verify equality between two array types.

        In order for two array types to be equal, they must have the same inner
        type and size. If the size of one of them is None, meaning that it
        wasn't specified in the declaration, then we only care abou the type
        of the arrays being equal.

        Examples:
            1. int a[] = {3, 4, 5};
               Here the type specifier doesn't have a size but the initializer
               does, so the declaration is still valid and the comparison
               should return true and modify the type specifier to include the
               size.
            2. int a[3] = {1, 2, 3};
               In case all parameters are specified, we should check that both
               objects are array of ints and have the same size of 3.
        """
        if isinstance(other, ArrayType):
            if self.type == other.type:

                # At least one of the objects must have a specified size.
                assert self.size is not None or other.size is not None

                if self.size is None:
                    self.size = other.size
                    return True
                if other.size is None:
                    other.size = self.size
                    return True

                # In case both sizes are specified, verify that they're the
                # same.
                return self.size == other.size
        elif isinstance(other, PtrType) and self.type == other.type:
            return True
        elif other is StringType and self.type == CharType:
            return True
        return False


class FuncType(uCType):
    """Function type class needs to be instantianted for each declaration."""

    def __init__(self, ret_type, param_types, param_names=None):
        """Initialize function type.

        Args:
            ret_type: The return type of the function
            param_types: List of uCTypes for the parameters that the function
                accepts.
            param_names: Optional list of strings with the name of each
                parameter. If there are no parameters this value has to be
                None.
        """
        self.ret_type = ret_type
        self.param_types = param_types
        self.param_names = param_names
        assert param_types[0] != VoidType or param_names is None
        super().__init__(None, unary_ops={"&"})

    @property
    def typename(self):
        """Return the typename of the function type."""
        typename = f"ret_{self.ret_type.typename} func("
        for param_type in self.param_types:
            typename += f" {param_type.typename} "
        typename += ")"
        self._typename = typename
        return self._typename

    @property
    def opname(self):
        """Return opcode name for function type."""
        self._opname = f"{self.ret_type.opname}"
        return self._opname

    def __eq__(self, other):
        """Verify equality between two function types."""
        if isinstance(other, FuncType):
            if self.ret_type != other.ret_type:
                return False

            if len(self.param_types) != len(other.param_types):
                return False

            for i in range(len(self.param_types)):
                if self.param_types[i] != other.param_types[i]:
                    return False
            return True
        return False
