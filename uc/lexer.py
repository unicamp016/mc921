# pylint: disable=invalid-name,no-self-use
"""This module defines and runs the uC language lexer.

In order to run the lexer, simply call the module specifying an input stream.
The stream can fed from the stdin or through an input file.

Examples:
    python lexer.py <input_file>
    cat <input_file> | python lexer.py
"""
import argparse
import sys

import ply.lex as lex


class UCLexer:
    """A lexer for the uC language.

    After building it with build(), set the input text with input(), and call
    token() to get new tokens. Otherwise, given a filename, simply call scan()
    to lex an entire source program.
    """

    def __init__(self):
        """Create the lexer for the uC language.

        Attributes:
            lexer: The PLY lexer object.
            last_token: The last scanned token by the lexer.
        """
        self.lexer = None
        # Keeps track of the last token returned from self.token()
        self.last_token = None

    def build(self, **kwargs):
        """Build the lexer from the specification.

        This method must be called after instantiating the object. This is
        important because it uses the object's dict definition to identify the
        tokens and their respective regex patterns.
        """
        self.lexer = lex.lex(object=self, **kwargs)

    def reset_lineno(self):
        """Reset the internal line number counter of the lexer."""
        self.lexer.lineno = 1

    def input(self, text):
        """Set the input for the lexer."""
        self.lexer.input(text)

    def token(self):
        """Retrieve the latest token from the input stream."""
        self.last_token = self.lexer.token()
        return self.last_token

    def find_tok_column(self, token):
        """Find the column of the token in its line."""
        last_cr = self.lexer.lexdata.rfind('\n', 0, token.lexpos)
        return token.lexpos - last_cr

    # Internal auxiliary methods
    def _error(self, msg, token):
        loc = self._make_tok_location(token)
        print(f"Lexical error: {msg} at {loc[0]}:{loc[1]}")
        self.lexer.skip(1)

    def _make_tok_location(self, token):
        return (token.lineno, self.find_tok_column(token))

    # Scanner (used only for test)
    def scan(self, filename):
        """Run the lexer on the given input file or stdin by default."""
        if filename:
            f = open(filename, 'r')
        else:
            f = sys.stdin
        self.lexer.input(f.read())
        for tok in self.lexer:
            print(tok)

    # Reserved keywords
    keywords = (
        'ASSERT', 'BREAK', 'CHAR', 'ELSE', 'FLOAT', 'FOR', 'IF',
        'INT', 'PRINT', 'READ', 'RETURN', 'VOID', 'WHILE',
    )

    keyword_map = {}
    for keyword in keywords:
        keyword_map[keyword.lower()] = keyword

    # All the tokens recognized by the lexer
    tokens = keywords + (
        # Identifiers
        'ID',

        # Constants
        'INT_CONST',
        'FLOAT_CONST',
        'CHAR_CONST',
        'STRING',

        # Binary operators
        'TIMES',
        'DIV',
        'MOD',
        'PLUS',
        'MINUS',
        'LT',
        'LTE',
        'GT',
        'GTE',
        'EQ',
        'NEQ',
        'AND',
        'OR',

        # Unary operators
        'ADDRESS',
        'NOT',

        # Unary expressions
        'PLUSPLUS',
        'MINUSMINUS',

        # Assignment operators
        'EQUALS',
        'MUL_AS',
        'DIV_AS',
        'MOD_AS',
        'INC_AS',
        'DEC_AS',

        # Punctuation and enclosures
        'COMMA',
        'SEMI',
        'LPAREN',
        'RPAREN',
        'LBRACE',
        'RBRACE',
        'LBRACKET',
        'RBRACKET',
    )

    # Rules
    t_TIMES = r'\*'
    t_DIV = r'/'
    t_MOD = r'%'
    t_PLUS = r'\+'
    t_MINUS = r'-'
    t_LT = r'<'
    t_LTE = r'<='
    t_GT = r'>'
    t_GTE = r'>='
    t_EQ = r'=='
    t_NEQ = r'!='
    t_AND = r'&&'
    t_OR = r'\|\|'
    t_ADDRESS = r'&'
    t_NOT = r'!'
    t_PLUSPLUS = r'\+\+'
    t_MINUSMINUS = r'--'
    t_EQUALS = r'='
    t_MUL_AS = r'\*='
    t_DIV_AS = r'/='
    t_MOD_AS = r'%='
    t_INC_AS = r'\+='
    t_DEC_AS = r'-='
    t_COMMA = r','
    t_SEMI = r';'
    t_LPAREN = r'\('
    t_RPAREN = r'\)'
    t_LBRACE = r'{'
    t_RBRACE = r'}'
    t_LBRACKET = r'\['
    t_RBRACKET = r'\]'
    t_ignore = ' \t\r'

    def t_newline(self, t):  # noqa
        r'\n+'
        t.lexer.lineno += len(t.value)

    def t_STRING(self, t):  # noqa
        r'".*?"'
        t.type = "STRING"
        return t

    def t_CHAR_CONST(self, t):  # noqa
        r"'.?'"
        t.type = "CHAR_CONST"
        return t

    def t_FLOAT_CONST(self, t):  # noqa
        r'([0-9]*\.[0-9]+) | ([0-9]+\.)'
        t.type = "FLOAT_CONST"
        return t

    def t_INT_CONST(self, t):  # noqa
        r'[0-9]+'
        t.type = "INT_CONST"
        return t

    def t_ID(self, t):   # noqa
        r'[a-zA-Z_][0-9a-zA-Z_]*'
        t.type = self.keyword_map.get(t.value, "ID")
        return t

    def t_ccomment(self, t):   # noqa
        r'/\*(.|\n)*?\*/'
        t.lexer.lineno += t.value.count('\n')

    def t_cppcomment(self, t):   # noqa
        r'//.*\n'
        t.lexer.lineno += 1

    # Error handling
    def t_error(self, t):
        """Handle unrecognized tokens in the stream."""
        if t.value[0] == '/':
            msg = "Unfinished C comment"
        elif t.value[0] == '"':
            msg = "Unmatched quote"
        else:
            msg = "Illegal character %s" % repr(t.value[0])
        self._error(msg, t)


def main():
    """Parse input stream and run the lexer for the given data."""
    parser = argparse.ArgumentParser(description="Run the uC lexer.")
    parser.add_argument(
        "filename", type=str, nargs="?", default=None,
        help="The filename with the uC code that should be tokenized. In case "
             "no argument filename is provided, stdin is used by default.")
    args = parser.parse_args()

    m = UCLexer()
    m.build()
    m.scan(args.filename)


if __name__ == '__main__':
    main()
