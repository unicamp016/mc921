"""Optimization module.

This module defines classes of different optimizations that can be performed
for a given dataflow analysis.
"""
import abc


class RegDefs(dict):
    """Dictionary subclass used to map registers to their defintions."""

    def update_item(self, key, val, block_level=True):
        """Update one of the register definitions in the dictionary.

        This function updates the value of a given target register according
        to the rules of constant propagation transfer function. The transfer
        function is monotonically increasing, meaning that we can only go from
        UNDEF -> CONST -> NAC, but never backwards. This function should behave
        differently if it's called at a block level or withink a block. At the
        block level, we want different constants to conflict and result in a
        NAC value, however, within a block, the second constant simply
        overrides the first.

        Args:
            key: The target register who's value we wish to update.
            val: The value used to update the target register.
            block_level: Whether the update is performed for a dictionary of
                definitions transferred within blocks or definitions inside
                a block.
        """
        # Comments assume the general form cur_val /| val = ?
        cur_val = self.get(key, "UNDEF")
        if not block_level:
            # Within a block, always perform an update for a new value.
            if val != "UNDEF":
                self[key] = val
        else:
            if val != "UNDEF":  # else cur_val /| UNDEF = cur_val
                if val == "NAC":
                    self[key] = val  # cur_val /\ NAC = NAC
                else:
                    if cur_val != "NAC":  # else NAC /| val = NAC
                        if cur_val == "UNDEF":
                            self[key] = val  # UNDEF /| val = val
                        else:
                            if cur_val != val:  # else c /| c = c
                                self[key] = "NAC"  # c1 /| c2 = NAC

    def __str__(self):
        """Return a string with a newline separating each element."""
        ret_str = "{\n"
        for key, val in self.items():
            ret_str += f"  {key}: {val}\n"
        ret_str += "}"
        return ret_str


class Optimization(abc.ABC):
    """Abstract optimization class."""

    def __init__(self, analysis):
        """Initialize the optimization with the precomputed analysis.

        Attributes:
            analysis: A DataflowAnalysis object with the computed gen, kill,
                ins, and outs sets.
        """
        self.analysis = analysis

    @abc.abstractmethod
    def optimize(self):
        """Perform the optimization on the CFG blocks."""
        raise NotImplementedError


class DeadCodeElim(Optimization):
    """Dead code elimination optimization."""

    def __init__(self, liveness, global_vars):
        """Initialize dead code elimination opt with liveness and globals."""
        super().__init__(liveness)
        self.global_vars = global_vars

    def _update_liveness(self, live_set, kill, gen):
        del_inst = False
        if kill is not None:
            if kill not in live_set:
                del_inst = True
            else:
                live_set -= {kill}
        if not del_inst and gen is not None:
            live_set = live_set.union(gen)
        return live_set, del_inst

    def optimize(self):
        """Perform the dead code elimination optimization."""
        for blk in self.analysis.cfg.blocks:
            del_pos = set()
            live_set = self.analysis.outs[blk].copy()
            for pos, inst in enumerate(reversed(blk.instructions)):
                opname = inst[0].split("_")[0]
                kill, gen = None, None
                if opname in ("print", "cbranch", "param", "return"):
                    if len(inst) > 1:
                        gen = {inst[1]}
                elif opname == "call":
                    # We can't kill the target even if it isn't used because
                    # we need to be conservative and assume that the call
                    # might modify global variables so it can't be touched.
                    gen = self.global_vars
                elif opname == "literal":
                    kill = inst[2]
                elif opname == "read":
                    kill = inst[1]
                elif opname == "store":
                    optype = inst[0].partition("_")[-1]
                    kill = inst[2]
                    gen = {inst[1]}
                    if "*" in optype:
                        kill = None
                elif opname in ("load", "fptosi", "sitofp", "not"):
                    kill = inst[2]
                    gen = {inst[1]}
                elif opname in ("add", "sub", "mul", "div", "mod", "lt", "le",
                                "ge", "gt", "eq", "ne", "and", "or"):
                    kill = inst[3]
                    gen = {inst[1], inst[2]}
                elif opname in "elem":
                    gen = {inst[1], inst[2], inst[3]}
                live_set, del_inst = self._update_liveness(
                    live_set,
                    kill=kill,
                    gen=gen,
                )
                if del_inst:
                    del_pos.add(len(blk.instructions) - pos - 1)

            # Compute remaning instructions
            new_instructions = []
            for pos, inst in enumerate(blk.instructions):
                if pos not in del_pos:
                    new_instructions.append(inst)
            blk.instructions = new_instructions


class ConstantProp(Optimization):
    """Constant propagation optimization."""

    def __init__(self, analysis, replace_store=False):
        """Initialize the constant propagation optimization."""
        super().__init__(analysis)
        self.replace_store = replace_store
        self.cfg = self.analysis.cfg
        self.block_defs = {}
        self.label_to_block = {b.label: b for b in self.cfg.blocks}
        self.invalidate = self._build_invalidate_dict()
        self.funcs = {
            "fptosi": int,
            "sitofp": float,
            "add": lambda x, y: x + y,
            "sub": lambda x, y: x - y,
            "mul": lambda x, y: x * y,
            "div_float": lambda x, y: x / y,
            "div_int": lambda x, y: x // y,
            "mod": lambda x, y: x % y,
            "lt": lambda x, y: x < y,
            "le": lambda x, y: x <= y,
            "ge": lambda x, y: x >= y,
            "gt": lambda x, y: x > y,
            "eq": lambda x, y: x == y,
            "ne": lambda x, y: x != y,
            "and": lambda x, y: x & y,
            "or": lambda x, y: x | y,
            "not": lambda x: not x,
        }

    def _build_invalidate_dict(self):
        """Create a dictionary with all blocks that should be invalidated.

        When an instruction is updated, we need to do another constant prop
        pass on the block. This function creates a map between temporaries and
        the set of blocks that they should invalidate. This set is basically
        all the blocks where this definition belongs to its IN set.
        """
        invalidate = {}
        for blk in self.cfg.blocks:
            for blabel, pos in self.analysis.ins[blk]:
                inst = self.cfg.inst_dict[blabel][pos]
                opname = inst[0].split("_")[0]
                if opname == "store":
                    source_reg = inst[1]
                    invalid_set = invalidate.get(source_reg, set())
                    invalid_set.add(blk)
                    invalidate[source_reg] = invalid_set
        return invalidate

    def optimize(self):
        """Apply constant propagation and folding to the CFG."""
        not_visited = set(self.cfg.blocks)
        while not_visited:
            # We still traverse the CFG in a breadth-first manner.
            node = not_visited.pop()

            # Before calling update_defs, we need to compute the values of the
            # local variable defs by looking at the in set.
            block_defs = RegDefs()
            for blabel, pos in self.analysis.ins[node]:
                inst = self.cfg.inst_dict[blabel][pos]
                opname = inst[0].split("_")[0]
                if opname == "store":
                    target = inst[2]
                    inst_block = self.label_to_block[blabel]
                    inst_block_defs = self.block_defs.get(inst_block, {})
                    source_val = inst_block_defs.get(inst[1], "UNDEF")
                elif opname == "global":
                    target = inst[1]
                    source_val = "UNDEF" if len(inst) == 2 else inst[2]
                else:
                    target = inst[1]
                    source_val = "NAC"
                block_defs.update_item(target, source_val, block_level=True)

            # Keep track of the previous definitions of local variables in each
            # block to see if anything changed after calling _update_defs.
            prev_block_defs = self.block_defs.get(node, {})

            # Update the definitions within the current block.
            new_block_defs = self._update_defs(node, block_defs)
            self.block_defs[node] = new_block_defs

            # In case any local variable definitions changed, make sure to
            # invalidate the visits to any block where this definition belongs
            # to the in set.
            not_visited = self._invalidate_visits(
                not_visited, new_block_defs, prev_block_defs)

        # Replace any instruction that evaluates to a constant by a
        # literal_type instruction.
        for block, bdefs in self.block_defs.items():
            for pos, inst in enumerate(block.instructions):
                opname = inst[0].split("_")[0]
                target = inst[-1]
                if opname == "store":
                    if self.replace_store:
                        target_val = bdefs.get(target, "UNDEF")
                        if target_val not in ("NAC", "UNDEF"):
                            type_tokens = inst[0].split("_")
                            # Only replace stores for basic types
                            if len(type_tokens) <= 2:
                                block.instructions[pos] = \
                                    (f"literal_{type_tokens[-1]}",
                                     bdefs[target],
                                     target)
                elif opname == "cbranch":
                    source_val = self.block_defs[block].get(inst[1], "UNDEF")
                    if source_val not in ("NAC", "UNDEF"):
                        jump_label = inst[2]
                        if not source_val:
                            jump_label = inst[3]
                        jump_inst = ("jump", jump_label)
                        block.instructions[pos] = jump_inst
                else:
                    typename = None
                    if opname in ("load", "add", "sub", "mul", "div", "mod"):
                        typename = inst[0].partition("_")[-1]
                    elif opname == "fptosi":
                        typename = "int"
                    elif opname == "sitofp":
                        typename = "float"
                    elif opname in ("lt", "le", "ge", "gt", "eq", "ne", "and",
                                    "or", "not"):
                        typename = "bool"
                    if typename is not None:
                        target_val = \
                            self.block_defs[block].get(target, "UNDEF")
                        if target_val not in ("NAC", "UNDEF"):
                            block.instructions[pos] = \
                                (f"literal_{typename}",
                                 self.block_defs[block][target],
                                 target)

    def _invalidate_visits(self, not_visited, new_defs, prev_defs):
        # First, we need to find out what definitions changed after the
        # previous block visit
        if prev_defs != new_defs:
            for key in new_defs:
                if key in prev_defs:
                    if prev_defs[key] != new_defs[key]:
                        not_visited = not_visited.union(
                            self.invalidate.get(key, set())
                        )
                else:
                    not_visited = not_visited.union(
                        self.invalidate.get(key, set())
                    )
        return not_visited

    def _update_defs(self, block, block_defs):
        for inst in block.instructions:
            opname = inst[0].split("_")[0]
            if opname == "literal":
                block_defs.update_item(inst[2], inst[1], block_level=False)
            if opname == "global":
                source_val = "UNDEF"
                if len(inst) == 3:
                    source_val = inst[2]
                block_defs.update_item(inst[1], source_val, block_level=False)
            elif opname == "load":
                target = inst[2]
                source_val = block_defs.get(inst[1], "UNDEF")
                block_defs.update_item(target, source_val, block_level=False)
            elif opname == "store":
                target = inst[2]
                source_val = block_defs.get(inst[1], "UNDEF")
                block_defs.update_item(target, source_val, block_level=False)
            elif opname == "read":
                target = inst[1]
                block_defs.update_item(target, "NAC", block_level=False)
            elif opname == "call":
                block_defs.update_item(inst[2], "NAC")
            elif opname in ("fptosi", "sitofp", "not"):
                source_val = block_defs.get(inst[1], "UNDEF")
                if source_val not in ("NAC", "UNDEF"):
                    source_val = self.funcs[opname](source_val)
                block_defs.update_item(inst[2], source_val, block_level=False)
            elif opname in ("add", "sub", "mul", "div", "mod", "lt", "le",
                            "ge", "gt", "eq", "ne", "and", "or"):
                source_val1 = block_defs.get(inst[1], "UNDEF")
                source_val2 = block_defs.get(inst[2], "UNDEF")
                if source_val1 == "NAC" or source_val2 == "NAC":
                    source_val = "NAC"
                elif source_val1 == "UNDEF" or source_val2 == "UNDEF":
                    source_val = "UNDEF"
                else:
                    if opname == "div":
                        opname = inst[0]
                    source_val = self.funcs[opname](source_val1, source_val2)
                block_defs.update_item(inst[3], source_val, block_level=False)
            elif opname == "elem":
                # We don't propagate constants through arrays.
                block_defs.update_item(inst[3], "NAC", block_level=False)
        return block_defs
