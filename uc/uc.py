#!/usr/bin/env python3
"""
    uc.py -- uC (a.k.a. micro C) language compiler.

============================================================
  This is the main program for the uc compiler, which just
  parses command-line options, figures out which source files
  to read and write to, and invokes the different stages of
  the compiler proper.
============================================================
"""
import argparse
import sys

from dataflow import optimize_code
from exprcheck import ProgramCheckVisitor
from exprcode import UCCodeGen
from exprerror import num_errors_reported, subscribe_errors, SemanticError
from interpreter import Interpreter
from llvmcode import LLVMCodeGen
from parser import UCParser


class Compiler:
    """Encapsulates the compiler and serves as an interfaces."""

    def __init__(self, input_args):
        """Initialize compiler object with zero errors and warnings."""
        self.total_errors = 0
        self.total_warnings = 0
        self.args = input_args
        self.open_files = {}
        self.code = None
        self.ast = None
        self.gencode = None
        self.optcode = None

    def _parse(self):
        """Parse the source code.

        If ast_file != None, or running at susy machine, prints out the
        abstract syntax tree.
        """
        ucparser = UCParser()
        self.ast = ucparser.parse(self.code, False)

    def _sema(self):
        """Decorate AST with semantic actions.

        If ast_file != None, or running at susy machine, prints out the
        abstract syntax tree.
        """
        try:
            sema_check = ProgramCheckVisitor()
            sema_check.visit(self.ast)
            if not self.args.susy and "ast_file" in self.open_files:
                self.ast.show(buf=self.open_files["ast_file"], showcoord=True)
        except SemanticError:
            pass

    def _codegen(self):
        """Generate uCIR Code for the decorated AST."""
        code_gen = UCCodeGen()
        code_gen.visit(self.ast)
        self.gencode = code_gen.code
        if not self.args.susy and "ir_file" in self.open_files:
            print(*self.gencode, sep="\n", file=self.open_files["ir_file"])

    def _opt(self):
        """Optimize the generated code."""
        self.optcode = optimize_code(self.gencode, llvm=self.args.llvm)
        if not self.args.susy and "opt_file" in self.open_files:
            print(*self.optcode, sep="\n", file=self.open_files["opt_file"])

    def _llvm(self):
        """Translate to llvm IR and execute."""
        code = self.optcode if self.args.opt else self.gencode
        llvm = LLVMCodeGen(self.args.filename, code)
        llvm.generate_ir(False)
        llvm.compile_ir(self.args.llvm_opts)
        if not self.args.susy and "llvm_file" in self.open_files:
            print(llvm, file=self.open_files["llvm_file"])
        if not self.args.no_run:
            llvm.run()

    def _do_compile(self):
        """Compiles the code to the given source file."""
        self._parse()
        if not num_errors_reported():
            self._sema()
        if not num_errors_reported():
            self._codegen()
            if self.args.opt:
                self._opt()
            if self.args.llvm:
                self._llvm()

    def compile(self):
        """Compiles the given filename."""
        if self.args.filename[-3:] == '.uc':
            filename = self.args.filename
        else:
            filename = self.args.filename + '.uc'

        if self.args.ast and not self.args.susy:
            ast_filename = filename[:-3] + '.ast'
            sys.stderr.write("Outputting the AST to %s.\n" % ast_filename)
            ast_file = open(ast_filename, 'w')
            self.open_files["ast_file"] = ast_file

        if self.args.ir and not self.args.susy:
            ir_filename = filename[:-3] + '.ir'
            sys.stderr.write("Outputting the uCIR to %s.\n" % ir_filename)
            ir_file = open(ir_filename, 'w')
            self.open_files["ir_file"] = ir_file

        if self.args.opt and not self.args.susy:
            opt_filename = filename[:-3] + '.opt'
            sys.stderr.write(
                "Outputting the optimized uCIR to %s.\n" % opt_filename)
            opt_file = open(opt_filename, 'w')
            self.open_files["opt_file"] = opt_file

        if self.args.llvm and not self.args.susy:
            llvm_filename = filename[:-3] + '.ll'
            sys.stderr.write("Outputting the LLVM IR to %s.\n" % llvm_filename)
            llvm_file = open(llvm_filename, 'w')
            self.open_files["llvm_file"] = llvm_file

        if self.args.llvm_opts and not self.args.susy:
            llvm_opt_filename = filename[:-3] + '.opt.ll'
            sys.stderr.write("Outputting the optimized LLVM IR to %s.\n"
                             % llvm_opt_filename)
            llvm_opt_file = open(llvm_opt_filename, 'w')
            self.open_files["llvm_opt_file"] = llvm_opt_file

        source = open(filename, 'r')
        self.code = source.read()
        source.close()

        with subscribe_errors(lambda msg: sys.stderr.write(msg+"\n")):
            self._do_compile()
            if num_errors_reported():
                sys.stderr.write(
                    "{} error(s) encountered.".format(num_errors_reported()))
            elif not self.args.llvm:
                if self.args.opt:
                    speedup = len(self.gencode) / len(self.optcode)
                    sys.stderr.write(
                        "original = %d, otimizado = %d, speedup = %.2f\n"
                        % (len(self.gencode), len(self.optcode), speedup))
                if not self.args.run and not self.args.cfg:
                    interp = Interpreter()
                    if self.args.opt:
                        interp.run(self.optcode)
                    else:
                        interp.run(self.gencode)

        for f in self.open_files.values():
            f.close()
        return 0


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    parser.add_argument("-s", "--susy", action='store_true',
                        help="Run in the susy machine")
    parser.add_argument("-a", "--ast", action='store_true',
                        help="Dump the AST in the 'filename'.ast")
    parser.add_argument("-i", "--ir", action='store_true',
                        help="Dump the uCIR in the 'filename'.ir")
    parser.add_argument("-n", "--no-run", action='store_true',
                        help="Do not execute the program")
    parser.add_argument("-c", "--cfg", action='store_true',
                        help="Show the CFG for each function in pdf format")
    parser.add_argument("-o", "--opt", action='store_true',
                        help="Optimize the uCIR with const prop and dce")
    parser.add_argument("-d", "--debug", action='store_true',
                        help="Print in the stderr some debug informations")
    parser.add_argument("-l", "--llvm", action='store_true',
                        help="Generate LLVM IR code in the 'filename'.ll")
    parser.add_argument("-p", "--llvm-opts",
                        nargs='*',
                        choices=['ctm', 'dce', 'cfg'],
                        help="Specify which llvm pass optimizations is "
                             "enabled")
    args = parser.parse_args()

    retval = Compiler(args).compile()
    sys.exit(retval)
