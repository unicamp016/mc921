"""Program checking module.

This module is responsible for the semantic validation of the program. It does
so by evaluating the AST output from the parser using the visitor pattern.
"""
import argparse
import sys

import astree as ast
from parser import UCParser
from exprerror import error, subscribe_errors, SemanticError
from exprtype import (ArrayType, BoolType, CharType, FloatType, FuncType,
                      IntType, PtrType, StringType, VoidType)


class SymbolTable:
    """Class representing a symbol table.

    Allows adding and looking up identifiers in the stack of symbol tables
    which representing a nested scope.
    """

    def __init__(self):
        """Initialize symbol table with root scope."""
        self.stack = [({}, None)]

    def push_scope(self, node):
        """Nest an additional scope to symbol table."""
        table = ({}, node)
        self.stack.append(table)

    def pop_scope(self):
        """Remove topmost scope from symbol table."""
        return self.stack.pop()

    def scope_level(self):
        """Return the scope level of the envirnoment."""
        return len(self.stack)

    def return_type(self):
        """Return the environment's current return type."""
        for _, node in reversed(self.stack):
            if isinstance(node, ast.FuncDef):
                return node.check_type
        return None

    def func_name(self):
        """Return the environment's current function name."""
        for _, node in reversed(self.stack):
            if isinstance(node, ast.FuncDef):
                return node.declarator.name.name
        return None

    def lookup_root(self, name):
        """Lookup symbol in the root scope of the environment."""
        return self.stack[0][0].get(name)

    def lookup_scope(self, name):
        """Lookup symbol in the topmost environemnt."""
        return self.stack[-1][0].get(name)

    def lookup(self, name):
        """Lookup symbol in the environment starting from innermost scope."""
        for table, _ in reversed(self.stack):
            value = table.get(name)
            if value is not None:
                return value
        return None

    def add(self, name, value):
        """Add a new symbol to topmost scope."""
        self.stack[-1][0][name] = value

    def add_root(self, name, value):
        """Add symbol to root scope."""
        self.stack[0][0][name] = value

    def __str__(self):
        """Return the string representation of the environment."""
        env_str = "-" * 30
        for i, table in enumerate(self.stack):
            env_str += f"\nScope {i+1}:\n"
            symbol_dict = {}
            for sym, val in table[0].items():
                symbol_dict[sym] = str(val.__class__)
            env_str += f"{symbol_dict}\n"
        env_str += "-" * 30
        return env_str


class ProgramCheckVisitor(ast.NodeVisitor):
    """Basic program checking according to uC's semantic rules."""

    def __init__(self):
        """Initialize program checking visitor with empty symbol table."""
        self.env = SymbolTable()
        self.types = {
            "int": IntType,
            "float": FloatType,
            "char": CharType,
            "bool": BoolType,
            "array": ArrayType,
            "string": StringType,
            "void": VoidType,
        }
        self.type_funcs = {
            "int": int,
            "float": float,
            "char": lambda s: s.strip('\''),
            "string": lambda s: s.strip('\"'),
        }
        self.break_scope = 0

    def visit_Program(self, node):
        """Visit the program node.

        Steps:
            1. Visit all of the global declarations.
            2. Make sure there's at least one function definition with name
               main.
        """
        for decl_node in node.gdecls:
            self.visit(decl_node)

        # By the end of the program, we should be back the root scope of the
        # symbol table.
        if self.env.lookup("main") is None:
            error(node.coord, 1,
                  "No function declaration 'main' is defined in the program.")

    def visit_FuncDef(self, node):
        """Visit the function definition node.

        Steps:
            1. Check that the function is defined in the root scope.
            2. Make sure there are no name conflicts for the definition.
            3. Add the function to the root symbol table.
            4. Assign the return type to the node's check type.
            5. Push a new scope for the function declarations.
            6. Visit the remaining children nodes, including the declarations
               and compound statement.
            7. Pop the scope as we exit the function definition.
        """
        if self.env.scope_level() > 1:
            error(node.coord, 2, "Unable to define nested functions.")
            return

        # Set the node's type to the function's return type.
        self.visit(node.type)
        node.check_type = node.type.check_type

        # Check if the function has been previously defined.
        func_name = node.declarator.name.name
        func_init_name = f"{func_name}_init"
        if self.env.lookup(func_init_name) is not None:
            error(node.coord, 24,
                  f"Function '{func_name}' has been previously defined.")
            return
        self.env.add(func_init_name, node)

        # Push a new scope for the function parameter declarations. The
        # function name is added to the root scope through a check made in
        # the Decl node.
        self.env.push_scope(node)

        # Visit the FuncDecl node directly in order to avoid calling the
        # visit_Decl method.
        self.visit(node.declarator.type)
        node.declarator.check_type = node.declarator.type.check_type

        # If there are any previous declarations, make sure that the types
        # match.
        func_decl_node = self.env.lookup_root(func_name)
        if func_decl_node is None:
            self.env.add_root(func_name, node.declarator)
        else:
            if node.declarator.check_type != func_decl_node.check_type:
                error(node.coord, 25,
                      f"Conflicting types in declaration of function "
                      f"'{func_name}'. Previous declaration had type "
                      f"{func_decl_node.check_type.typename}, while current "
                      f"declaration has type "
                      f"{node.declarator.check_type.typename}.")
                return

        for decl in node.decls:
            self.visit(decl)
        self.visit(node.comp_stm)
        self.env.pop_scope()

    def visit_FuncCall(self, node):
        """Visit function call node.

        Steps:
            1. Check that the call is made within a function body.
            2. Lookup the function name in the symbol table and throw an error
               in case it isn't found or the symbol doesn't correspond to a
               function
            3. Compare the types of the argument list of the parameter list.
            4. Propagate the function's return type as the function call's
               check type.
        """
        self.visit(node.pexpr)
        func_name = node.pexpr.name
        func_node = self.env.lookup(func_name)

        is_func_ptr = isinstance(func_node.check_type, PtrType) and \
            isinstance(func_node.check_type.type, FuncType)
        is_func = isinstance(func_node.check_type, FuncType)
        if not is_func and not is_func_ptr:
            error(node.coord, 22,
                  f"Tried to call non-function symbol '{func_name}'.")
            return

        func_node_type = func_node.check_type
        if is_func_ptr:
            func_node_type = func_node.check_type.type

        arg_expr_type = FuncType(ret_type=func_node_type.ret_type,
                                 param_types=[VoidType])
        if node.arg_expr is not None:
            self.visit(node.arg_expr)

            # When there's a single expression, we don't visit the ExprList
            # node, so we have to make a list of arguments ourselves.
            arg_types = node.arg_expr.check_type
            if not isinstance(arg_types, list):
                arg_types = [arg_types]

            arg_expr_type = FuncType(ret_type=func_node_type.ret_type,
                                     param_types=arg_types)

        if arg_expr_type != func_node_type:
            error(node.coord, 3,
                  f"Arguments type {arg_expr_type.typename} doesn't "
                  f"match parameters type "
                  f"{func_node_type.typename} in function "
                  f"call to '{func_name}'.")

        node.check_type = func_node_type.ret_type

    def visit_ArrayRef(self, node):
        """Visit the array reference node.

        Steps:
            1. Visit the postfix expression to determine if it is an array
               type.
            2. Check that the expr evaluates to an integer.
            3. Update the node's type to be the same as the elements of the
               array.
        """
        self.visit(node.pexpr)
        if not isinstance(node.pexpr.check_type, ArrayType):
            error(node.coord, 4,
                  f"Attempting to reference a non-array type. Type "
                  f"{node.pexpr.check_type.typename} isn't an array.")
            node.check_type = node.pexpr.check_type
            return

        self.visit(node.expr)
        if node.expr.check_type != IntType:
            error(node.coord, 5,
                  f"Array reference expression must evaluate to an integer. "
                  f"Type {node.expr.check_type.typename} isn't an integer.")
        node.check_type = node.pexpr.check_type.type

    def visit_If(self, node):
        """Visit the if and else statement node.

        Steps:
            1. Check that expr is of type bool.
            2. Visit the if statement.
            3. Visit the else statement in case it exists.
        """
        self.visit(node.expr)
        if node.expr.check_type != BoolType:
            error(node.coord, 6,
                  f"Type mismatch in if statement expression. Expected "
                  f"boolean and got {node.expr.check_type.typename}.")
        self.visit(node.if_stm)
        if node.else_stm is not None:
            self.visit(node.else_stm)

    def visit_Assert(self, node):
        """Visit assert statement node.

        Steps:
            1. Check that the expression is of type bool.
        """
        self.visit(node.expr)
        if node.expr.check_type != BoolType:
            error(node.coord, 7,
                  f"Assert statement expected boolean expression. Given "
                  f"expression is of type {node.expr.check_type.typename}.")

    def visit_Break(self, node):
        """Visit break statement node.

        Steps:
            1. Check if the statement is inside a for or while looop.
        """
        assert self.break_scope >= 0
        if self.break_scope == 0:
            error(node.coord, 23,
                  "Break statement should be inside a for or while loop.")

    def visit_Return(self, node):
        """Visit return node.

        Steps:
            1. If there is a return expression available, visit it.
            2. Check that the return type matches the current environment's
               expected return type.
        """
        ret_type = VoidType
        if node.expr is not None:
            self.visit(node.expr)
            ret_type = node.expr.check_type

        if ret_type != self.env.return_type():
            error(node.coord, 8,
                  f"Return statement type mismatch in function "
                  f"'{self.env.func_name()}'. Expected "
                  f"{self.env.return_type().typename} and got "
                  f"{ret_type.typename}.")

    def visit_While(self, node):
        """Visit the while iteration node.

        Steps:
            1. Ensure that expr is a boolean.
            2. Visit the statement without creating a new scope (specific to
               uC).
        """
        self.break_scope += 1
        self.visit(node.expr)
        if node.expr.check_type != BoolType:
            error(node.coord, 9,
                  f"Type mismatch in while statement expression. Expected "
                  f"boolean and got {node.expr.check_type.typename}.")
        self.visit(node.statement)
        self.break_scope -= 1

    def visit_For(self, node):
        """Visit the for statement node.

        Steps:
            1. Create a new scope in case expr1 declares new variables which
               shouldn't conflict with the ones declared inside the compound
               statement.
            2. Visit each of the expressions and make sure that the middle one
               is of type bool.
            3. Create a new scope for the statement and then visit it.
            4. Pop both scopes before exiting the node.
        """
        self.env.push_scope(node)
        self.break_scope += 1

        if node.expr1 is not None:
            self.visit(node.expr1)
        if node.expr2 is not None:
            self.visit(node.expr2)
            if node.expr2.check_type != BoolType:
                error(node.coord, 10,
                      f"Type mismatch in second expression of for statement. "
                      f"Expected boolean and got "
                      f"{node.expr2.check_type.typename}.")
        if node.expr3 is not None:
            self.visit(node.expr3)

        self.visit(node.statement)

        self.break_scope -= 1
        self.env.pop_scope()

    def visit_GlobalDecl(self, node):
        """Visit global declaration node.

        The main difference between global and local declarations is that a
        global symbol can be declared multiple times, but it can be defined
        only once. We can make this distinction by storing whether or not a
        given symbol has been defined in the symbol table. The only requirement
        is that multiple declarations must have matching types.
        """
        for decl in node.decls:
            decl_name = decl.name.name

            # Create a new scope for the parameters of the function
            # declaration.
            if isinstance(decl.type, ast.FuncDecl):
                self.env.push_scope(decl)
                self.visit(decl.type)
                self.env.pop_scope()
            else:
                self.visit(decl.type)
            decl.check_type = decl.type.check_type

            lookup_node = self.env.lookup(decl_name)
            if lookup_node is not None:
                # If the current node has already been declared, make sure
                # that both types match.
                if lookup_node.check_type != decl.type.check_type:
                    error(node.coord, 26,
                          f"Conflicting types in declaration of symbol "
                          f"'{decl_name}'. Previous declaration has type "
                          f"{lookup_node.check_type.typename}, while "
                          f"current declaration has type "
                          f"{decl.type.check_type.typename}.")
                    return
            else:
                # The name hasn't been declared before, so add it to the
                # symbol table.
                self.env.add(decl_name, decl)

            if decl.init is not None:
                if isinstance(decl.type, ast.FuncDecl):
                    error(node.coord, 27,
                          f"Function '{decl_name}' is initialized like a "
                          f"variable")
                    return
                # If we have a non function declaration with an initializer,
                # we just need to make sure that it hasn't been initialized
                # before. If it hasn't, we flag that it now has.
                self.visit(decl.init)
                decl_init_name = f"{decl_name}_init"
                lookup_init_node = self.env.lookup(decl_init_name)
                if lookup_init_node is not None:
                    error(node.coord, 28,
                          f"Symbol '{decl_name}' has been previously "
                          f"defined.")
                    return
                if decl.init.check_type != decl.type.check_type:
                    error(node.coord, 12,
                          f"Incompatible types in definition of symbol "
                          f"'{decl_name}'. Type "
                          f"{decl.type.check_type.typename} != "
                          f"{decl.init.check_type.typename}")
                    return
                self.env.add(decl_init_name, decl.init)

    def visit_Decl(self, node):
        """Visit the declaration node.

        Steps:
            1. Check that a declaration with the same name doesn't exist in the
               current scope.
            2. Add the declaration to the topmost scope.
            3. Verify that the declaration type is the same as the initializer
               type.
            4. Propagate the node's type upwards.
        """
        decl_name = node.name.name

        if self.env.lookup_scope(decl_name) is not None:
            error(node.coord, 11,
                  f"Symbol '{decl_name}' has already been declared in "
                  f"this scope.")
            node.check_type = VoidType
            return

        self.env.add(decl_name, node)

        # Create a new scope for the function declaration parameters.
        if isinstance(node.type, ast.FuncDecl):
            self.env.push_scope(node)
            self.visit(node.type)
            self.env.pop_scope()
        else:
            self.visit(node.type)

        node.check_type = node.type.check_type

        if node.init is not None:
            if isinstance(node.type, ast.FuncDecl):
                error(node.coord, 27,
                      f"Function '{decl_name}' is initialized like a "
                      f"variable")
                return
            self.visit(node.init)
            if node.type.check_type != node.init.check_type:
                error(node.coord, 12,
                      f"Incompatible types in definition of symbol "
                      f"'{decl_name}'. Type {node.type.check_type.typename} "
                      f"!= {node.init.check_type.typename}")

                # We still need to set check_type for error recovery, so we opt
                # for the type specifier's type.
                node.check_type = node.type.check_type
                return

    def visit_FuncDecl(self, node):
        """Visit the function declaration node.

        Steps:
            1. Push a new scope to the environment for the declarations made
               within the parameter list. This scope is popped at the end of
               the FuncDef visitor.
            2. Visit the return type to build the new FuncType check_type.
            3. Visit the parameter list to build the new FuncType.

        We don't care about the type node from FuncDecl, since we already know
        the return type of the function from the FuncDef node. The propagated
        check_type should be a FuncType from the uCTypes, which allows function
        call type checking to be made with a simple comparison.
        """
        self.visit(node.type)
        if node.params is not None:
            self.visit(node.params)
            param_names, param_types = list(zip(*node.params.check_type))
            node.check_type = FuncType(ret_type=node.type.check_type,
                                       param_types=param_types,
                                       param_names=param_names)
        else:
            node.check_type = FuncType(ret_type=node.type.check_type,
                                       param_types=[VoidType])

    def visit_ArrayDecl(self, node):
        """Visit the array declaration node.

        Steps:
            1. If there is a constant expression, verify that it is an integer.
            2. Create a new array type for the declaration from the array's
               type and size.
        """
        self.visit(node.type)

        size = None
        if node.const_expr is not None:
            self.visit(node.const_expr)
            if node.const_expr.check_type != self.types["int"]:
                error(node.coord, 13,
                      "Array declaration constant expression must be an "
                      "integer.")
                node.check_type = ArrayType(node.type.check_type, size=None)
                return
            size = int(node.const_expr.value)
        node.check_type = ArrayType(node.type.check_type, size=size)

    def visit_PtrDecl(self, node):
        """Visit pointer declaration node.

        Steps:
            1. Visit the node's type
            2. Create a new pointer type that points to the node's type.
        """
        self.visit(node.type)
        node.check_type = PtrType(ptr_type=node.type.check_type)

    def visit_VarDecl(self, node):
        """Visit variable declaration node.

        Steps:
            1. Propagate the check type.
        """
        self.visit(node.type)
        node.check_type = node.type.check_type

    def visit_ParamList(self, node):
        """Visit the parameter list node.

        Steps:
            1. Visit all of the declaration nodes.
            2. Return the list of types of the parameters in order for it to
               be used when creating a FuncType object in the FuncDecl visitor.
        """
        for param in node.params:
            self.visit(param)
        node.check_type = [(param.name.name, param.check_type)
                           for param in node.params]

    def visit_InitList(self, node):
        """Visit initializer list node.

        Steps:
            1. Verify that all of the items in the list have the same type.
            2. Create a new array type from the size and type of the list.
            3. Propagate a list the the values of the initializer list for
               later use in the code generating phase.
        """
        for init in node.inits:
            self.visit(init)

        init_list_types = [init.check_type for init in node.inits]
        first_init_type = init_list_types[0]
        for init_type in init_list_types[1:]:
            if first_init_type != init_type:
                error(node.coord, 15,
                      f"Type mismatch in initializer list. Type "
                      f"{init_type.typename} != {first_init_type.typename}.")
                break
        node.check_type = ArrayType(first_init_type, size=len(node.inits))

        # Compute values for code generation phase.
        node.gen_value = [init.gen_value for init in node.inits]

    def visit_ExprList(self, node):
        """Visit expression list node.

        Steps:
            1. Visit all of the expression nodes.
            2. Return a list of uCTypes as the check_type in order to build the
               FuncType object in the FuncCall node.
        """
        for expr in node.exprs:
            self.visit(expr)
        node.check_type = [expr.check_type for expr in node.exprs]

    def visit_BinaryOp(self, node):
        """Visit binary op node.

        Steps:
            1. Visit each side of the binary op to compute their types.
            2. Ensure that both types are the same.
            3. If the op is within the set of binary_ops, we assign the same
               type as the operand.
            4. If the op belongs to the set of rel_ops, we assign the type bool
               to this node.
        """
        self.visit(node.lvalue)
        self.visit(node.rvalue)
        ltype = node.lvalue.check_type
        rtype = node.rvalue.check_type

        # By default we assign the LHS's type to the binary op's check_type
        node.check_type = ltype

        if ltype != rtype:
            error(node.coord, 16,
                  f"Type mismatch in binary op '{node.op}'. Type "
                  f"{ltype.typename} != {rtype.typename}.")
            return

        if node.op in ltype.binary_ops:
            return

        if node.op in ltype.rel_ops:
            node.check_type = BoolType
            return

        # If the op isn't supported by the type, throw an error.
        error(node.coord, 17,
              f"Binary operator '{node.op}' not supported by type "
              f"{ltype.typename}.")

    def visit_UnaryOp(self, node):
        """Visit unary op node.

        Steps:
            1. Visit the expr to compute its type.
            2. Check that the type supports the given op.
        """
        self.visit(node.expr)
        node.check_type = node.expr.check_type
        if node.op not in node.expr.check_type.unary_ops:
            error(node.coord, 18,
                  f"Unary op '{node.op}' not supported by type "
                  f"{node.expr.check_type.typename}")
            return

        if node.op == '*':
            assert isinstance(node.check_type, PtrType)
            node.check_type = node.check_type.type
            return
        if node.op == '&':
            node.check_type = PtrType(ptr_type=node.check_type)

    def visit_Cast(self, node):
        """Vist cast node.

        Steps:
            1. Visit the type node and update the cast node's type to the
               new cast type.
        """
        self.visit(node.type)
        self.visit(node.cast_expr)
        node.check_type = node.type.check_type

    def visit_Assignment(self, node):
        """Visit assignment node.

        Steps:
            1. Visit each side of the assignment expression.
            2. Check that both types of the assignment match.
            3. Check that the assignment type supports the given assignment op.
        """
        self.visit(node.unary_expr)
        self.visit(node.assign_expr)
        node.check_type = node.unary_expr.check_type
        if node.unary_expr.check_type != node.assign_expr.check_type:
            error(node.coord, 19,
                  f"Type mismatch in expression assignment '{node.op}'. "
                  f"Expected {node.unary_expr.check_type.typename} and got "
                  f"{node.assign_expr.check_type.typename}.")
            return
        if node.op not in node.unary_expr.check_type.assign_ops:
            error(node.coord, 20,
                  f"Assignment op '{node.op}' not supported by type "
                  f"{node.unary_expr.check_type.typename}")

    def visit_Type(self, node):
        """Visit type node.

        Assign one of the singleton type objects given the type of the node.
        """
        node.check_type = self.types[node.names[0]]

    def visit_Constant(self, node):
        """Visit constant node.

        The syntax of uC already guarantees that the node's type will match its
        value, so we just need to propagate the check_type.
        """
        node.check_type = self.types[node.type]
        node.gen_value = self.type_funcs[node.type](node.value)

    def visit_ID(self, node):
        """Visit identifier node.

        Steps:
            1. Lookup the identifier in the environment and update it's type
               in case it's been declared.
            2. If it hasn't been declared, throw an error. The only cases where
               we don't want to throw an error is when we're making
               declarations. In these cases, we don't need to visit the ID
               node, we can just perform a lookup with it's name and
               appropriately handle cases of name conflicts.
        """
        symbol = self.env.lookup(node.name)
        if symbol is not None:
            node.check_type = symbol.check_type
            return
        error(node.coord, 21,
              f"No identifier with name '{node.name}' is defined.")
        node.check_type = VoidType


def main():
    """Parse input stream and check the program for the given source code."""
    parser = argparse.ArgumentParser(description="Run the uC program checker.")
    parser.add_argument(
        "filename", type=str, nargs="?", default=None,
        help="The filename with the uC code that should be checked. In case "
             "no argument filename is provided, stdin is used by default.")
    args = parser.parse_args()

    if args.filename:
        file = open(args.filename, 'r')
    else:
        file = sys.stdin

    ucparser = UCParser()
    ast_root = ucparser.parse(input_code=file.read())

    if ucparser.nerrors == 0:
        with subscribe_errors(lambda msg: sys.stdout.write(msg + '\n')):
            try:
                prog_check = ProgramCheckVisitor()
                prog_check.visit(ast_root)
            except SemanticError:
                pass


if __name__ == "__main__":
    main()
