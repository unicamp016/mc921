"""This module defines the nodes for the abstract syntax tree.

The abstract base class Node implements the printing functionality for
visualizng the syntax tree. The only required method that every subclass must
implement is children() which is used to iterate over the nodes in order to
print the full tree. This module's classes are used with the UCParser to build
the tree.
"""
import abc
import sys


class Node(abc.ABC):
    """Base class for the AST nodes.

    By default, instances of classes have a dictionary for attribute storage.
    This wastes space for objects having very few instance variables. The space
    consumption can become acute when creating large numbers of instances.

    The default can be overridden by defining __slots__ in a class definition.
    The __slots__ declaration takes a sequence of instance variables and
    reserves just enough space in each instance to hold a value for each
    variable. Space is saved because __dict__ is not created for each instance.
    """

    SHOWCOORD_BLACKLIST = None

    __slots__ = ('coord', 'check_type', 'gen_location', 'gen_value',
                 'gen_addr')

    attr_names = ()

    def __init__(self, coord=None):
        """Initialize a node with a coord object."""
        self.coord = coord
        self.check_type = None
        self.gen_location = None
        self.gen_value = None
        self.gen_addr = False

    @abc.abstractmethod
    def children(self):
        """Return a list of all children that are Nodes."""
        raise NotImplementedError

    def show(self, buf=sys.stdout, offset=0, attrnames=False, nodenames=False,
             showcoord=False, _my_node_name=None):
        """Print the node and all its attributes and children recursively.

        Arguments:
            buf: Open IO buffer into which the Node is printed.
            offset: Initial offset (amount of leading spaces)
            attrnames: True if you want to see the attribute names in
                name=value pairs. False to only see the values.
            nodenames: True if you want to see the actual node names within
                their parents.
            showcoord: Do you want the coordinates of each Node to be
                displayed.
        """
        lead = ' ' * offset
        if nodenames and _my_node_name is not None:
            buf.write(
                lead + self.__class__.__name__ + ' <' + _my_node_name + '>: ')
        else:
            buf.write(lead + self.__class__.__name__ + ': ')

        if self.attr_names:
            if attrnames:
                nvlist = [(n, getattr(self, n)) for n in self.attr_names
                          if getattr(self, n) is not None]
                attrstr = ', '.join('%s=%s' % nv for nv in nvlist)
            else:
                vlist = [getattr(self, n) for n in self.attr_names]
                attrstr = ', '.join('%s' % v for v in vlist)
            buf.write(attrstr)

        if showcoord and not isinstance(self, Node.SHOWCOORD_BLACKLIST):
            if self.coord:
                buf.write('%s' % self.coord)
        buf.write('\n')

        for (child_name, child) in self.children():
            child.show(buf, offset + 4, attrnames, nodenames, showcoord,
                       child_name)

    def __repr__(self):
        """Generate a python representation of the current node."""
        result = self.__class__.__name__ + '('
        indent = ''
        separator = ''
        for name in self.__slots__:
            result += separator
            result += indent
            result += name + '='
            nr_spaces = len(name) + len(self.__class__.__name__)
            result += _repr(getattr(self, name)).replace(
                '\n',
                '\n  ' + (' ' * nr_spaces))
            separator = ','
            indent = ' ' * len(self.__class__.__name__)
        result += indent + ')'
        return result


def _repr(obj):
    """Get the representation of a node object."""
    if isinstance(obj, list):
        return '[' + (',\n '.join((_repr(e).replace('\n', '\n ')
                                   for e in obj))) + '\n]'
    return repr(obj)


class NodeVisitor(object):
    """A base NodeVisitor class for visiting uc_ast nodes.

    Subclass it and define your own visit_XXX methods, where XXX is the class
    name you want to visit with these methods.

    For example:

    class ConstantVisitor(NodeVisitor):
        def __init__(self):
            self.values = []

        def visit_Constant(self, node):
            self.values.append(node.value)

    Creates a list of values of all the constant nodes
    encountered below the given node. To use it:

    cv = ConstantVisitor()
    cv.visit(node)

    Notes:
    *   generic_visit() will be called for AST nodes for which
        no visit_XXX method was defined.
    *   The children of nodes for which a visit_XXX was
        defined will not be visited - if you need this, call
        generic_visit() on the node.
        You can use:
            NodeVisitor.generic_visit(self, node)
    *   Modeled after Python's own AST visiting facilities
        (the ast module of Python 3.0)

    """

    _method_cache = None

    def visit(self, node):
        """Visit a node."""
        if self._method_cache is None:
            self._method_cache = {}

        visitor = self._method_cache.get(node.__class__.__name__, None)
        if visitor is None:
            method = 'visit_' + node.__class__.__name__
            visitor = getattr(self, method, self.generic_visit)
            self._method_cache[node.__class__.__name__] = visitor

        return visitor(node)

    def generic_visit(self, node):
        """Call if no explicit visitor function exists for a node.

        Implements preorder visiting of the node.
        """
        for _, child in node.children():
            self.visit(child)


class Program(Node):
    """Root node of the abstract syntax tree."""

    __slots__ = ('gdecls',)

    def __init__(self, gdecls, coord=None):
        """Initialize program node with list of global declarations."""
        super().__init__(coord)
        self.gdecls = gdecls

    def children(self):
        """Return chilren nodes."""
        nodelist = []
        for i, child in enumerate(self.gdecls or []):
            nodelist.append(("gdecls[%d]" % i, child))
        return tuple(nodelist)


class GlobalDecl(Node):
    """Global declaration node."""

    __slots__ = ('decls',)

    def __init__(self, decls, coord=None):
        """Initialize global declaration with a declaration."""
        super().__init__(coord)
        self.decls = decls

    def children(self):
        """Return child node."""
        nodelist = [("gdecl%d" % i, decl) for i, decl in enumerate(self.decls)]
        return tuple(nodelist)


class FuncDef(Node):
    """Function definition node."""

    __slots__ = ('declarator', 'decls', 'comp_stm', 'type')

    def __init__(self, declarator, decls, comp_stm, ret_type=None, coord=None):
        """Initialize function definition node."""
        super().__init__(coord)
        self.declarator = declarator
        self.decls = decls
        self.comp_stm = comp_stm
        self.type = ret_type

    def children(self):
        """Return all children."""
        nodelist = []
        nodelist.append(("type", self.type))
        nodelist.append(("declarator", self.declarator))
        nodelist.extend([("decl%d" % i, decl)
                         for i, decl in enumerate(self.decls)])
        nodelist.append(("compound_stm", self.comp_stm))
        return tuple(nodelist)


class FuncCall(Node):
    """Function call node."""

    __slots__ = ('pexpr', 'arg_expr')

    def __init__(self, pexpr, arg_expr, coord=None):
        """Initialize for node with postfix expression and argument expr."""
        super().__init__(coord)
        self.pexpr = pexpr
        self.arg_expr = arg_expr

    def children(self):
        """Return nodelist with exprs."""
        nodelist = [("pexpr", self.pexpr)]
        if self.arg_expr:
            nodelist.append(("arg_expr", self.arg_expr))
        return tuple(nodelist)


class ArrayRef(Node):
    """Array reference node."""

    __slots__ = ('pexpr', 'expr')

    def __init__(self, pexpr, expr, coord=None):
        """Initialize for node with postfix expression and expression."""
        super().__init__(coord)
        self.pexpr = pexpr
        self.expr = expr

    def children(self):
        """Return nodelist with exprs."""
        nodelist = [("pexpr", self.pexpr), ("expr", self.expr)]
        return tuple(nodelist)


class Compound(Node):
    """Compound statement node."""

    __slots__ = ('decls', 'stms')

    def __init__(self, decls, stms, coord=None):
        """Initialize a compound statement."""
        super().__init__(coord)
        self.decls = decls
        self.stms = stms

    def children(self):
        """Return the node's expr and statements."""
        nodelist = []
        if self.decls:
            nodelist.extend([("decl%d" % i, decl)
                             for i, decl in enumerate(self.decls)])
        if self.stms:
            nodelist.extend([("stm%d" % i, stm)
                             for i, stm in enumerate(self.stms)])
        return tuple(nodelist)


class EmptyStatement(Node):
    """Empty statement node."""

    def children(self):
        """Return empty nodelist."""
        return ()


class If(Node):
    """If and else statement node."""

    __slots__ = ('expr', 'if_stm', 'else_stm')

    def __init__(self, expr, if_stm, else_stm=None, coord=None):
        """Initialize an if statement."""
        super().__init__(coord)
        self.expr = expr
        self.if_stm = if_stm
        self.else_stm = else_stm

    def children(self):
        """Return the node's expr and statements."""
        nodelist = [("expr", self.expr), ("if_stm", self.if_stm)]
        if self.else_stm:
            nodelist.append(("else_stm", self.else_stm))
        return tuple(nodelist)


class Assert(Node):
    """Assert statement node."""

    __slots__ = ('expr',)

    def __init__(self, expr, coord=None):
        """Initialize a assert statement with an expression."""
        super().__init__(coord)
        self.expr = expr

    def children(self):
        """Return the given expr."""
        nodelist = [("expr", self.expr)]
        return tuple(nodelist)


class Read(Node):
    """Read node."""

    __slots__ = ('arg_expr',)

    def __init__(self, arg_expr, coord=None):
        """Initialize read node with argument expression list."""
        super().__init__(coord)
        self.arg_expr = arg_expr

    def children(self):
        """Return nodelist with argument expression."""
        nodelist = [("arg_expr", self.arg_expr)]
        return tuple(nodelist)


class Break(Node):
    """Break node."""

    def children(self):
        """Return empty nodelist."""
        return ()


class Return(Node):
    """Return node."""

    __slots__ = ('expr',)

    def __init__(self, expr, coord=None):
        """Initialize return node with nullable expression."""
        super().__init__(coord)
        self.expr = expr

    def children(self):
        """Return nodelist with expression in case it exists."""
        nodelist = []
        if self.expr:
            nodelist.append(("expr", self.expr))
        return tuple(nodelist)


class While(Node):
    """While iteration node."""

    __slots__ = ('expr', 'statement')

    def __init__(self, expr, statement, coord=None):
        """Initialize while node with expr and statement."""
        super().__init__(coord)
        self.expr = expr
        self.statement = statement

    def children(self):
        """Return nodelist with expr and statement."""
        nodelist = [("expr", self.expr), ("statement", self.statement)]
        return tuple(nodelist)


class For(Node):
    """For iteration node."""

    __slots__ = ('expr1', 'expr2', 'expr3', 'statement')

    def __init__(self, expr1, expr2, expr3, statement, coord=None):
        """Initialize for node with list of exprs and statement."""
        super().__init__(coord)
        self.expr1 = expr1
        self.expr2 = expr2
        self.expr3 = expr3
        self.statement = statement

    def children(self):
        """Return nodelist with exprs and statement."""
        nodelist = []
        if self.expr1:
            nodelist.append(("expr1", self.expr1))
        if self.expr2:
            nodelist.append(("expr2", self.expr2))
        if self.expr3:
            nodelist.append(("expr2", self.expr3))
        nodelist.append(("statement", self.statement))
        return tuple(nodelist)


class DeclList(Node):
    """Declaration list node."""

    __slots__ = ('decls',)

    def __init__(self, decls, coord=None):
        """Initialize declaration list."""
        super().__init__(coord)
        self.decls = decls

    def children(self):
        """Return nodelist with the list of declarations."""
        nodelist = [("decl%d" % i, decl) for i, decl in enumerate(self.decls)]
        return tuple(nodelist)


class Decl(Node):
    """Declaration node."""

    __slots__ = ('type', 'name', 'init')

    def __init__(self, decl_type, name=None, initializer=None, coord=None):
        """Initialize declaration with type and optional name and init_expr."""
        super().__init__(coord)
        self.type = decl_type
        self.name = name
        self.init = initializer

    def children(self):
        """Return nodelist with type and list of params."""
        nodelist = [("type", self.type)]
        if self.init:
            nodelist.append(("initializer", self.init))
        return tuple(nodelist)

    attr_names = ('name',)


class FuncDecl(Node):
    """Function declaration node."""

    __slots__ = ('type', 'params')

    def __init__(self, decl_type, params, coord=None):
        """Initialize function declaration with type and list of params.

        The list of params could either be a ParamList object or a python list
        of identifiers that can be empty. Both cases should be handled in this
        class.
        """
        super().__init__(coord)
        self.type = decl_type
        self.params = params

    def children(self):
        """Return nodelist with type and list of params."""
        nodelist = []
        if self.params is not None:
            nodelist.append(("paramlist", self.params))
        nodelist.append(("type", self.type))
        return tuple(nodelist)


class ArrayDecl(Node):
    """Array declaration node."""

    __slots__ = ('type', 'const_expr')

    def __init__(self, decl_type, const_expr=None, coord=None):
        """Initialize array declaration with type and constant expression."""
        super().__init__(coord)
        self.type = decl_type
        self.const_expr = const_expr

    def children(self):
        """Return nodelist with type and constant expression."""
        nodelist = [("type", self.type)]
        if self.const_expr:
            nodelist.append(("const_expr", self.const_expr))
        return tuple(nodelist)


class PtrDecl(Node):
    """Pointer declaration node."""

    __slots__ = ('type', 'declname')

    def __init__(self, decl_type=None, coord=None):
        """Initialize pointer declaration with declaration type."""
        super().__init__(coord)
        self.type = decl_type

    def children(self):
        """Return the type declaration as the child."""
        nodelist = [("type", self.type)]
        return tuple(nodelist)


class VarDecl(Node):
    """Basic variable declaration node."""

    __slots__ = ('type', 'declname')

    def __init__(self, decl_name, decl_type=None, coord=None):
        """Initialize basic variable declaration with type specifier and id."""
        super().__init__(coord)
        self.declname = decl_name
        self.type = decl_type

    def children(self):
        """Return only the declaration type as a child."""
        nodelist = [("type", self.type)]
        return tuple(nodelist)


class ParamList(Node):
    """Parameter list node."""

    __slots__ = ('params',)

    def __init__(self, params, coord=None):
        """Initialize parameter list node with list of declarations."""
        super().__init__(coord)
        self.params = params

    def children(self):
        """Return the declaration nodes as a tuple."""
        nodelist = [('param%d' % i, param)
                    for i, param in enumerate(self.params)]
        return tuple(nodelist)


class InitList(Node):
    """Initializer list node."""

    __slots__ = ('inits',)

    def __init__(self, inits, coord=None):
        """Initialize init list node with a list of initializers."""
        super().__init__(coord)
        self.inits = inits

    def children(self):
        """Return the tuple with all initializers."""
        nodelist = [("init%d" % i, init) for i, init in enumerate(self.inits)]
        return tuple(nodelist)


class ExprList(Node):
    """Expression list node."""

    __slots__ = ('exprs',)

    def __init__(self, exprs, coord=None):
        """Initialize expression list node with a list of expressions."""
        super().__init__(coord)
        self.exprs = exprs

    def children(self):
        """Return the list of experessions as children."""
        return tuple(("expr%d" % i, expr) for i, expr in enumerate(self.exprs))


class Print(Node):
    """Print statement node."""

    __slots__ = ('expr',)

    def __init__(self, expr, coord=None):
        """Initialize a print statement with an expression."""
        super().__init__(coord)
        self.expr = expr

    def children(self):
        """Return the given expr in case it exists."""
        nodelist = []
        if self.expr:
            nodelist.append(("expression", self.expr))
        return tuple(nodelist)


class BinaryOp(Node):
    """Binary operation node."""

    __slots__ = ('op', 'lvalue', 'rvalue')

    def __init__(self, op, left, right, coord=None):
        """Initialize binary op node with op, left, and right values."""
        super().__init__(coord)
        self.op = op
        self.lvalue = left
        self.rvalue = right

    def children(self):
        """Return all children nodes."""
        nodelist = [("lvalue", self.lvalue), ("rvalue", self.rvalue)]
        return tuple(nodelist)

    attr_names = ('op',)


class UnaryOp(Node):
    """Unary operator node."""

    __slots__ = ('op', 'expr')

    def __init__(self, op, expr, coord=None):
        """Initialize unary operator with op string."""
        super().__init__(coord)
        self.op = op
        self.expr = expr

    def children(self):
        """Return tuple with the expression associated to the unary op."""
        nodelist = [("expression", self.expr)]
        return tuple(nodelist)

    attr_names = ('op',)


class Cast(Node):
    """Cast expression node."""

    __slots__ = ('type', 'cast_expr')

    def __init__(self, cast_type, cast_expr, coord=None):
        """Initialize a cast node with type specifier and cast expression."""
        super().__init__(coord)
        self.type = cast_type
        self.cast_expr = cast_expr

    def children(self):
        """Return a list with the cast type and expr nodes as children."""
        nodelist = [("type", self.type), ("expression", self.cast_expr)]
        return tuple(nodelist)


class Assignment(Node):
    """Assignment operator node."""

    __slots__ = ('op', 'unary_expr', 'assign_expr')

    def __init__(self, op, unary_expr, assign_expr, coord=None):
        """Initialize assignment op node with op string."""
        super().__init__(coord)
        self.op = op
        self.unary_expr = unary_expr
        self.assign_expr = assign_expr

    def children(self):
        """Return children of the assignment node."""
        nodelist = [("unary_expr", self.unary_expr),
                    ("assign_expr", self.assign_expr)]
        return tuple(nodelist)

    attr_names = ('op',)


class Type(Node):
    """Type specifier node."""

    __slots__ = ('names',)

    def __init__(self, names, coord=None):
        """Initialize type specifier with a list of modifier names."""
        super().__init__(coord)
        self.names = names

    def children(self):
        """Return empty set of children since type is a leaf node."""
        return ()

    attr_names = ('names',)


class ID(Node):
    """Identifier node."""

    __slots__ = ('name',)

    def __init__(self, name, coord=None):
        """Initialize identifier node with its name."""
        super().__init__(coord)
        self.name = name

    def children(self):
        """Return empty tuple since id is a leaf node."""
        return ()

    attr_names = ('name',)


class Constant(Node):
    """Constant node."""

    __slots__ = ('type', 'value')

    def __init__(self, const_type, value, coord=None):
        """Initialize constant node with type and value."""
        super().__init__(coord)
        self.type = const_type
        self.value = value

    def children(self):
        """Return empty tuple since this node is a leaf in the tree."""
        return ()

    attr_names = ('type', 'value')


# Tuple of classes that shouldn't have their coordinates displayed in order for
# the program's output to be compatible with the expected one.
Node.SHOWCOORD_BLACKLIST = (
    Program,
    ParamList,
    GlobalDecl,
    Decl,
    FuncDecl,
    ArrayDecl,
    VarDecl,
    FuncDef,
)
