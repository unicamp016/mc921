# pylint: disable=invalid-name,no-self-use
"""Parser for the uC language.

This module defines the UCParser class which specifies the grammar rules for
the uC language. The module also defines the auxiliary Coord class to add line
number and column index information to each node in the abstract syntax tree
built through the parser.
"""
import argparse
import sys

import ply.yacc as yacc

import astree as ast
import lexer


class UCParser:
    """Parser for the uC language."""

    tokens = lexer.UCLexer.tokens

    precedence = (
        ('right', 'THEN', 'ELSE'),
        ('left', 'OR'),
        ('left', 'AND'),
        ('left', 'EQ', 'NEQ'),
        ('left', 'LT', 'GT', 'LTE', 'GTE'),
        ('left', 'PLUS', 'MINUS'),
        ('left', 'TIMES', 'DIV', 'MOD'),
    )

    def __init__(self):
        """Initialize uC language parser."""
        self.nerrors = 0
        self.uclexer = lexer.UCLexer()
        self.uclexer.build()
        self._format_docstrings()

    def _token_coord(self, p, token_idx):
        """Return the coordinate object for a given token from the lexer."""
        last_cr = p.lexer.lexer.lexdata.rfind('\n', 0, p.lexpos(token_idx))
        if last_cr < 0:
            last_cr = -1
        column = (p.lexpos(token_idx) - (last_cr))
        return Coord(p.lineno(token_idx), column)

    def _format_docstrings(self):
        """Format docstrings to allow PLY yacc to parse them."""
        for name, func in UCParser.__dict__.items():
            if name.startswith("p_"):
                docs = func.__doc__
                rules = docs.split('|')

                new_rules = []
                for r in rules:
                    new_rules.append(r.replace('\n', ''))

                func.__doc__ = '\n|'.join(new_rules)

    def _build_expr_list(self, p):
        if len(p) == 2:
            p[0] = p[1]
        else:
            if not isinstance(p[1], ast.ExprList):
                p[1] = ast.ExprList([p[1]], p[1].coord)
            p[1].exprs.append(p[3])
            p[0] = p[1]

    def _build_declarations(self, spec, decls):
        """Build a list of declarations sharing the given specifiers."""
        declarations = []

        for decl in decls:
            assert decl['decl'] is not None
            declaration = ast.Decl(
                name=None,
                decl_type=decl['decl'],
                initializer=decl.get('init'),
                coord=decl['decl'].coord)
            fixed_decl = self._fix_decl_name_type(declaration, spec)
            declarations.append(fixed_decl)

        return declarations

    def _fix_decl_name_type(self, decl, typename):
        """Fix a declaration. Modifies decl."""
        # Reach the underlying basic type
        decl_type = decl
        while not isinstance(decl_type, ast.VarDecl):
            decl_type = decl_type.type

        decl.name = decl_type.declname

        if not typename:
            # Functions default to returning int
            if not isinstance(decl.type, ast.FuncDecl):
                self._parse_error("Missing type in declaration", decl.coord)
            decl_type.type = ast.Type(['int'], coord=decl.coord)
        else:
            # At this point, we know that typename is a list of Type
            # nodes. Concatenate all the names into a single list.
            decl_type.type = typename
        return decl

    def _type_modify_decl(self, decl, modifier):
        """Tacks a type modifier on a declarator.

        Returns the modified declarator.
        """
        modifier_head = modifier
        modifier_tail = modifier

        # The modifier may be a nested list. Reach its tail.
        while modifier_tail.type:
            modifier_tail = modifier_tail.type

        # If the decl is a basic type, just tack the modifier onto it
        if isinstance(decl, ast.VarDecl):
            modifier_tail.type = decl
            return modifier

        # Otherwise, the decl is a list of modifiers. Reach
        # its tail and splice the modifier onto the tail,
        # pointing to the underlying basic type.
        decl_tail = decl

        while not isinstance(decl_tail.type, ast.VarDecl):
            decl_tail = decl_tail.type

        modifier_tail.type = decl_tail.type
        decl_tail.type = modifier_head
        return decl

    def _parse_error(self, *args, **kwargs):
        raise ValueError(*args, **kwargs)

    def p_program(self, p):  # noqa
        """program : global_declaration_list"""
        p[0] = ast.Program(p[1], coord=Coord(1, 1))

    def p_global_declaration_list(self, p):  # noqa
        """global_declaration_list : global_declaration
                                   | global_declaration_list global_declaration
        """
        p[0] = [p[1]] if len(p) == 2 else p[1] + [p[2]]

    def p_global_declaration_1(self, p):  # noqa
        """global_declaration : declaration"""
        p[0] = ast.GlobalDecl(p[1], coord=p[1][0].coord)

    def p_global_declaration_2(self, p):  # noqa
        """global_declaration : function_definition"""
        p[0] = p[1]

    def p_function_definition(self, p):  # noqa
        """function_definition : declarator decl_list_opt compound_statement
        """
        fixed_decl = self._build_declarations(None, p[1])[0]
        ret_type = fixed_decl.type.type
        p[0] = ast.FuncDef(fixed_decl, p[2], p[3], ret_type,
                           coord=p[1][0]['decl'].coord)

    def p_function_definition_type(self, p):  # noqa
        """function_definition : type_specifier declarator decl_list_opt
                                 compound_statement
        """
        fixed_decl = self._build_declarations(p[1], p[2])[0]
        p[0] = ast.FuncDef(fixed_decl, p[3], p[4], p[1], coord=p[1].coord)

    def p_statement_list_opt(self, p):  # noqa
        """statement_list_opt : statement_list
                              | empty
        """
        p[0] = p[1] if p[1] is not None else []

    def p_statement_list(self, p):  # noqa
        """statement_list : statement_list statement
                          | statement
        """
        p[0] = [p[1]] if len(p) == 2 else p[1] + [p[2]]

    def p_statement(self, p):  # noqa
        """statement : expression_statement
                     | compound_statement
                     | selection_statement
                     | iteration_statement
                     | jump_statement
                     | assert_statement
                     | print_statement
                     | read_statement
        """
        p[0] = p[1]

    def p_compound_statement(self, p):  # noqa
        """compound_statement : LBRACE decl_list_opt statement_list_opt RBRACE
        """
        p[0] = ast.Compound(p[2], p[3], coord=Coord(p.lineno(1), 1))

    def p_expression_statement(self, p):  # noqa
        """expression_statement : expression_opt SEMI"""
        if p[1] is None:
            p[0] = ast.EmptyStatement(coord=self._token_coord(p, 2))
        else:
            p[0] = p[1]

    def p_if_statement(self, p):  # noqa
        """selection_statement : IF LPAREN expression RPAREN
                                 statement %prec THEN
                               | IF LPAREN expression RPAREN
                                 statement ELSE statement
        """
        else_stm = None
        if len(p) == 8:
            else_stm = p[7]
        p[0] = ast.If(p[3], p[5], else_stm, coord=self._token_coord(p, 1))

    def p_assert_statement(self, p):  # noqa
        """assert_statement : ASSERT expression SEMI"""
        p[0] = ast.Assert(p[2], coord=self._token_coord(p, 1))

    def p_read_statement(self, p):  # noqa
        """read_statement : READ LPAREN argument_expression RPAREN SEMI"""
        p[0] = ast.Read(p[3], coord=self._token_coord(p, 1))

    def p_break_statement(self, p):  # noqa
        """jump_statement : BREAK SEMI"""
        p[0] = ast.Break(coord=self._token_coord(p, 1))

    def p_return_statement(self, p):  # noqa
        """jump_statement : RETURN expression_opt SEMI"""
        p[0] = ast.Return(p[2], coord=self._token_coord(p, 1))

    def p_while_statement(self, p):  # noqa
        """iteration_statement : WHILE LPAREN expression RPAREN statement"""
        p[0] = ast.While(p[3], p[5], coord=self._token_coord(p, 1))

    def p_for_statement_expr(self, p):  # noqa
        """iteration_statement : FOR LPAREN expression_opt SEMI expression_opt
                                 SEMI expression_opt RPAREN statement
        """
        p[0] = ast.For(p[3], p[5], p[7], p[9], coord=self._token_coord(p, 1))

    def p_for_statement_decl(self, p):  # noqa
        """iteration_statement : FOR LPAREN declaration expression_opt
                                 SEMI expression_opt RPAREN statement
        """
        decls = ast.DeclList(p[3], coord=self._token_coord(p, 1))
        p[0] = ast.For(decls, p[4], p[6], p[8], coord=self._token_coord(p, 1))

    def p_declaration_list_opt(self, p):  # noqa
        """decl_list_opt : decl_list
                         | empty
        """
        p[0] = p[1] if p[1] is not None else []

    def p_declaration_list(self, p):  # noqa
        """decl_list : declaration
                     | decl_list declaration
        """
        p[0] = p[1] if len(p) == 2 else p[1] + p[2]

    def p_declaration(self, p):  # noqa
        """declaration : type_specifier init_declarator_list SEMI
                       | type_specifier SEMI
        """
        if len(p) == 3:
            p[0] = [p[1]]
        else:
            p[0] = self._build_declarations(p[1], p[2])

    def p_init_declarator_list(self, p):  # noqa
        """init_declarator_list : init_declarator
                                | init_declarator_list COMMA init_declarator
        """
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[1].extend(p[3])
            p[0] = p[1]

    def p_init_declarator(self, p):  # noqa
        """init_declarator : declarator
                           | declarator EQUALS initializer
        """
        if len(p) == 4:
            p[1][0]['init'] = p[3]
        p[0] = p[1]

    def p_declarator(self, p):  # noqa
        """declarator : direct_declarator"""
        p[0] = [{'decl': p[1]}]

    def p_pointer_declarator(self, p):  # noqa
        """declarator : pointer direct_declarator"""
        ptr_decl = self._type_modify_decl(decl=p[2], modifier=p[1])
        p[0] = [{'decl': ptr_decl}]

    def p_pointer(self, p):  # noqa
        """pointer : TIMES pointer_opt"""
        p[0] = ast.PtrDecl(decl_type=p[2], coord=self._token_coord(p, 1))

    def p_pointer_opt(self, p):  # noqa
        """pointer_opt : pointer
                       | empty
        """
        p[0] = p[1]

    def p_direct_declarator_func(self, p):  # noqa
        """direct_declarator : direct_declarator LPAREN parameter_list RPAREN
                             | direct_declarator LPAREN identifier_list_opt
                               RPAREN
        """
        func_decl = ast.FuncDecl(
            decl_type=None,
            params=p[3],
            coord=p[1].coord
        )

        p[0] = self._type_modify_decl(decl=p[1], modifier=func_decl)

    def p_direct_declarator_array(self, p):  # noqa
        """direct_declarator : direct_declarator LBRACKET
                               constant_expression_opt RBRACKET
        """
        arr_decl = ast.ArrayDecl(
            decl_type=None,
            const_expr=p[3],
            coord=p[1].coord
        )

        p[0] = self._type_modify_decl(decl=p[1], modifier=arr_decl)

    def p_direct_declarator_var(self, p):  # noqa
        """direct_declarator : identifier
                             | LPAREN declarator RPAREN
        """
        if len(p) == 2:
            p[0] = ast.VarDecl(p[1], coord=p[1].coord)
        else:
            p[0] = p[2][0]['decl']

    def p_identifier_list_opt(self, p):  # noqa
        """identifier_list_opt : identifier_list
                               | empty
        """
        p[0] = p[1]

    def p_identifier_list(self, p):  # noqa
        """identifier_list : identifier_list COMMA identifier
                           | identifier
        """
        if len(p) == 2:
            p[0] = ast.ParamList([p[1]], p[1].coord)
        else:
            p[1].params.append(p[3])
            p[0] = p[1]

    def p_parameter_list(self, p):  # noqa
        """parameter_list : parameter_declaration
                          | parameter_list COMMA parameter_declaration
        """
        if len(p) == 2:
            p[0] = ast.ParamList(p[1], p[1][0].coord)
        else:
            p[1].params.extend(p[3])
            p[0] = p[1]

    def p_parameter_declaration(self, p):  # noqa
        """parameter_declaration : type_specifier declarator"""
        p[0] = self._build_declarations(p[1], p[2])

    def p_constant_expression_opt(self, p):  # noqa
        """constant_expression_opt : constant_expression
                                   | empty
        """
        p[0] = p[1]

    def p_constant_expression(self, p):  # noqa
        """constant_expression : binary_expression"""
        p[0] = p[1]

    def p_initializer(self, p):  # noqa
        """initializer : assignment_expr
                       | LBRACE initializer_list RBRACE
                       | LBRACE initializer_list COMMA RBRACE
        """
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = ast.InitList(p[2], p[2][0].coord)

    def p_initializer_list(self, p):  # noqa
        """initializer_list : initializer
                            | initializer_list COMMA initializer
        """
        p[0] = [p[1]] if len(p) == 2 else p[1] + [p[3]]

    def p_print_statement(self, p):  # noqa
        """print_statement : PRINT LPAREN expression_opt RPAREN SEMI
        """
        if len(p) == 6:
            p[0] = ast.Print(p[3], self._token_coord(p, 1))

    def p_argument_expression_opt(self, p):  # noqa
        """argument_expression_opt : argument_expression
                                   | empty
        """
        p[0] = p[1]

    def p_argument_expression(self, p):  # noqa
        """argument_expression : assignment_expr
                               | argument_expression COMMA assignment_expr
        """
        self._build_expr_list(p)

    def p_expression_opt(self, p):  # noqa
        """expression_opt : expression
                          | empty
        """
        p[0] = p[1]

    def p_expression(self, p):  # noqa
        """expression : assignment_expr
                      | expression COMMA assignment_expr
        """
        self._build_expr_list(p)

    def p_assignment_expression(self, p):  # noqa
        """assignment_expr : binary_expression
                           | unary_expression assignment_op assignment_expr
        """
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = ast.Assignment(p[2], p[1], p[3], p[1].coord)

    def p_binary_expression(self, p):  # noqa
        """binary_expression : cast_expression
                             | binary_expression TIMES binary_expression
                             | binary_expression DIV binary_expression
                             | binary_expression MOD binary_expression
                             | binary_expression PLUS binary_expression
                             | binary_expression MINUS binary_expression
                             | binary_expression LT binary_expression
                             | binary_expression LTE binary_expression
                             | binary_expression GT binary_expression
                             | binary_expression GTE binary_expression
                             | binary_expression EQ binary_expression
                             | binary_expression NEQ binary_expression
                             | binary_expression AND binary_expression
                             | binary_expression OR binary_expression
        """
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = ast.BinaryOp(p[2], p[1], p[3], p[1].coord)

    def p_cast_expression(self, p):  # noqa
        """cast_expression : LPAREN type_specifier RPAREN cast_expression
                           | unary_expression
        """
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = ast.Cast(p[2], p[4], self._token_coord(p, 1))

    def p_unary_expression(self, p):  # noqa
        """unary_expression : PLUSPLUS unary_expression
                            | MINUSMINUS unary_expression
                            | unary_operator cast_expression
                            | postfix_expression
        """
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = ast.UnaryOp(p[1], p[2], p[2].coord)

    def p_func_call(self, p):  # noqa
        """postfix_expression : postfix_expression LPAREN
                                argument_expression_opt RPAREN
        """
        p[0] = ast.FuncCall(p[1], p[3], coord=p[1].coord)

    def p_array_ref(self, p):  # noqa
        """postfix_expression : postfix_expression LBRACKET expression
                                RBRACKET
        """
        p[0] = ast.ArrayRef(p[1], p[3], coord=p[1].coord)

    def p_postfix_expression(self, p):  # noqa
        """postfix_expression : postfix_expression PLUSPLUS
                              | postfix_expression MINUSMINUS
                              | primary_expression
        """
        if len(p) == 2:
            p[0] = p[1]
        else:
            op_name = "p%s" % (p[2])
            p[0] = ast.UnaryOp(op_name, p[1], p[1].coord)

    def p_primary_expression_paren(self, p):  # noqa
        """primary_expression : LPAREN expression RPAREN"""
        p[0] = p[2]

    def p_primary_expression(self, p):  # noqa
        """primary_expression : identifier
                              | constant
                              | STRING
        """
        if isinstance(p[1], str):
            p[0] = ast.Constant('string', p[1], self._token_coord(p, 1))
        else:
            p[0] = p[1]

    def p_unary_operator(self, p):  # noqa
        """unary_operator : ADDRESS
                          | TIMES
                          | PLUS
                          | MINUS
                          | NOT
        """
        p[0] = p[1]

    def p_assignment_operator(self, p):  # noqa
        """assignment_op : EQUALS
                         | MUL_AS
                         | DIV_AS
                         | MOD_AS
                         | INC_AS
                         | DEC_AS
        """
        p[0] = p[1]

    def p_identifier(self, p):  # noqa
        """identifier : ID"""
        p[0] = ast.ID(p[1], self._token_coord(p, 1))

    def p_constant_int(self, p):  # noqa
        """constant : INT_CONST"""
        p[0] = ast.Constant("int", p[1], self._token_coord(p, 1))

    def p_constant_float(self, p):  # noqa
        """constant : FLOAT_CONST"""
        p[0] = ast.Constant("float", p[1], self._token_coord(p, 1))

    def p_constant_char(self, p):  # noqa
        """constant : CHAR_CONST"""
        p[0] = ast.Constant("char", p[1], self._token_coord(p, 1))

    def p_type_specifier(self, p):  # noqa
        """type_specifier : VOID
                          | CHAR
                          | INT
                          | FLOAT
        """
        p[0] = ast.Type([p[1]], self._token_coord(p, 1))

    def p_empty(self, p):  # noqa
        """empty :"""
        p[0] = None

    def p_error(self, p):
        """Error recovery method for PLY.

        Here we just want to print the symbol that caused the error, since no
        error recovery is implemented.
        """
        self.nerrors += 1
        if p is not None:
            print(f"{p.lineno}:{p.lexpos}: Syntax error near symbol {p.value}")
        else:
            print("Syntax error at the end of input")

    def parse(self, input_code, debug=False):
        """Parse the input and output an abstract syntax tree."""
        parser = yacc.yacc(module=self, write_tables=False, debug=False,
                           errorlog=uCLogger(sys.stderr))
        ast_root = parser.parse(input=input_code, lexer=self.uclexer,
                                debug=debug)
        return ast_root


class uCLogger(yacc.PlyLogger):
    """Custom logger for uC parser.

    All it does is ignore the debug messages.
    """

    def debug(self, msg, *args, **kwargs):
        # pylint: disable=unused-argument
        """Noop."""
        return


class Coord():
    """Coordinates of a syntactic element.

    Consists of a line number and an optional column number.
    """

    __slots__ = ('line', 'column')

    def __init__(self, line, column=None):
        """Initialize coordinate object with line number and column index."""
        self.line = line
        self.column = column

    def __str__(self):
        """Return string representation of token coordinate."""
        if self.line:
            coord_str = "   @ %s:%s" % (self.line, self.column)
        else:
            coord_str = ""
        return coord_str


def main():
    """Parse input stream and run the parser for the given data."""
    parser = argparse.ArgumentParser(description="Run the uC parser.")
    parser.add_argument(
        "filename", type=str, nargs="?", default=None,
        help="The filename with the uC code that should be tokenized. In case "
             "no argument filename is provided, stdin is used by default.")
    args = parser.parse_args()

    if args.filename:
        file = open(args.filename, 'r')
    else:
        file = sys.stdin

    ucparser = UCParser()
    ast_root = ucparser.parse(input_code=file.read())
    if ucparser.nerrors == 0:
        ast_root.show(showcoord=True)


if __name__ == "__main__":
    main()
