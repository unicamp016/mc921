"""Code generator module.

This module generates the intermediate representation for the given source
code based on Single Static Assignment (SSA).
"""
import argparse
import copy
import sys

import astree as ast
from exprcheck import ProgramCheckVisitor, SymbolTable
from exprerror import num_errors_reported, subscribe_errors, SemanticError
from exprtype import StringType, ArrayType, PtrType, VoidType
from parser import UCParser


def format_instruction(inst):
    """Format the raw instruction tuple to a neat string for visualization."""
    inst_str = ""
    opname = inst[0].split("_")[0]
    if opname == "define":
        ret_type = inst[0].split("_")[1]
        func_name = inst[1]
        inst_str += f"\ndefine {ret_type} {func_name}("
        for param_idx, (param_type, param_name) in enumerate(inst[2:]):
            if param_idx > 0:
                inst_str += ", "
            inst_str += f"{param_type} {param_name}"
        inst_str += ")"
    elif len(inst) == 1 and opname not in ("print", "return"):
        inst_str += f"{inst[0]}"
    elif opname == "global":
        global_type = inst[0].split("_")[1]
        if global_type == "string":
            inst_str += f"{inst[0]} {inst[1]}, \'{inst[2]}\'"
        else:
            for token in inst:
                inst_str += f"{token} "
    elif opname == "cbranch":
        inst_str += f"  {inst[0]} {inst[1]} label {inst[2]} label {inst[3]}"
    elif opname == "jump":
        inst_str += f"  {inst[0]} label {inst[1]}"
    else:
        inst_str += "  "
        for token in inst:
            inst_str += f"{token} "
    return inst_str


class UCCodeGen(ast.NodeVisitor):
    """Node visitor class that creates 3-address instruction sequences."""

    BINARY_OPS = {
        "+": "add",
        "-": "sub",
        "*": "mul",
        "/": "div",
        "%": "mod",
        "<": "lt",
        ">": "gt",
        "<=": "le",
        ">=": "ge",
        "==": "eq",
        "!=": "ne",
        "&&": "and",
        "||": "or",
    }

    def __init__(self):
        """Initialize a code generator instance.

        Attributes:
            fname: Keeps track of the current scope's function name to
                determine what temporaries to increment when a new one is
                needed.
            versions: Dictionary that tracks the number of temporaries
                allocated for each function scope.
            code: The list of tuples with 3-address instructions.
            global_consts: List of constant initializer instructions. This is
                used to store strings and array initializers in the beginning
                of the program.
        """
        super().__init__()
        self.fname = 'main'
        self.versions = {self.fname: {}}
        self.code = []
        self.global_consts = []
        self.break_stack = []
        self.env = SymbolTable()

    def __str__(self):
        """Print the instruction sequence in a readable format."""
        code_str = ""
        for inst in self.code:
            code_str += f"{format_instruction(inst)}\n"
        return code_str.strip()

    def new_temp(self, name=".count"):
        """Create a new temporary variable with the given name.

        If name is .count, we use successive integers as the targets.
        Otherwise, we use the name appended to the count (e.g. x1) when count
        is >= 1.
        """
        if self.fname not in self.versions:
            self.versions[self.fname] = {}
        self.versions[self.fname][name] = \
            self.versions[self.fname].get(name, -1) + 1
        if name == ".count":
            target_name = f"%{self.versions[self.fname][name]}"
        else:
            target_name = f"%{name}"
            cnt = self.versions[self.fname][name]
            if cnt > 0:
                target_name += f"{cnt}"
        return target_name

    def get_label(self, address):
        """Return the label for a given address."""
        return f"{address.split('%')[-1]}:"

    def store_global_const(self, opname, value):
        """Store the given value as a global constant.

        Args:
            opname: A string with the type of the value in the form expected
                by the opcodes in the instruction set.
            value: The constant value that should be stored.
        Returns:
            A string with the address name used to refer to the constant.
        """
        const_addr = f"@.str.{len(self.global_consts)}"
        self.global_consts.append((
            f"global_{opname}",
            const_addr,
            value
        ))
        return const_addr

    def visit_Program(self, node):
        """Visit the global declaration nodes and prepend the constants."""
        for gdecl in node.gdecls:
            self.visit(gdecl)
        self.code = self.global_consts + self.code

    def visit_GlobalDecl(self, node):
        """Visit global declaration node.

        We don't generate any instructions for function declarations.
        """
        for decl in node.decls:
            decl_name = decl.name.name
            inst = None
            if decl.init is not None:
                inst = (f"global_{decl.check_type.opname}",
                        f"@{decl_name}",
                        decl.init.gen_value)
            else:
                inst = (f"global_{decl.check_type.opname}",
                        f"@{decl_name}")
            decl.gen_location = f"@{decl_name}"
            self.env.add(decl_name, decl)
            if not isinstance(decl.type, ast.FuncDecl):
                self.code.append(inst)

    def visit_Constant(self, node):
        """Append literal SSA instruction."""
        if node.type == "string":
            node.gen_location = self.store_global_const(
                opname="string",
                value=node.gen_value
            )
        else:
            target = self.new_temp()
            inst = ('literal_' + node.type,
                    node.gen_value,
                    target)
            self.code.append(inst)
            node.gen_location = target

    def visit_ID(self, node):
        """Visit identifier node.

        If we're visiting an identifier node, this means that the symbol
        has already been declared and should be available in our environment.
        """
        load_node = self.env.lookup(node.name)
        assert load_node is not None

        # If generate address is true, we have to return the address of the
        # identifier, instead of its value.
        if node.gen_addr:
            node.gen_location = load_node.gen_location
        else:
            target = self.new_temp()
            inst = (f"load_{load_node.check_type.opname}",
                    load_node.gen_location,
                    target)
            self.code.append(inst)
            node.gen_location = target

    def visit_Decl(self, node):
        """Visit a declaration node."""
        target = self.new_temp(node.name.name)

        # If we have a string initializer, make sure to populate the size
        # attribute of the array type.
        if node.init is not None and node.init.check_type is StringType:
            assert isinstance(node.check_type, ArrayType)
            node.check_type.size = len(node.init.gen_value)

        # Add instruction to allocate space for the new declaration
        inst = (f"alloc_{node.check_type.opname}", target)
        self.code.append(inst)

        # Add mapping from declaration name to target to environment.
        self.env.add(node.name.name, node)

        # Save the name of the temporary variable.
        node.gen_location = target

        # If the initializer is an array or a string, we store it's value
        # as a global constant and update the gen_location to the format used
        # for global consts (e.g. @.str.1)
        if node.init is not None:
            init_gen_location = None
            if isinstance(node.init.check_type, ArrayType):
                init_gen_location = self.store_global_const(
                    opname=node.init.check_type.opname,
                    value=node.init.gen_value
                )
            else:
                self.visit(node.init)
                init_gen_location = node.init.gen_location
            inst = (f"store_{node.check_type.opname}",
                    init_gen_location,
                    target)
            self.code.append(inst)

    def visit_BinaryOp(self, node):
        """Append binary or relational operation to instruction list."""
        # Visit the left and right expressions
        self.visit(node.lvalue)
        self.visit(node.rvalue)

        # Make a new temporary for storing the result
        target = self.new_temp()

        # Create the opcode and append to list
        opcode = self.BINARY_OPS[node.op] + "_" + node.lvalue.check_type.opname
        inst = (opcode,
                node.lvalue.gen_location,
                node.rvalue.gen_location,
                target)
        self.code.append(inst)

        # Store location of the result on the node
        node.gen_location = target

    def visit_FuncDef(self, node):
        """Append function definition instructions.

        Steps:
            1. Define the function's name.
            2. Make new temporaries for each parameter in the list.
            3. Make a new temporary for the return value.
            4. Allocate on stack each of the arguments, creating new
               temporaries for them.
            5. Create a new temporary for the return value.
            6. Store the values passed by registers in the newly allocated
               memory addresses in the stack.
            7. Allocate a new temporary with the return address of the
               function.
            8. Visit the compound statement node.
            9. Jump to the return address of the function.
            10. Execute instruction with just the number of the temporary of
               the return address?
            11. If the return type isn't void, load value from the return
                temporary into a new temporary.
            12. Return the new temporary.
        """
        # Add function definition.
        self.fname = node.declarator.name.name

        # Add function name to root scope.
        node.gen_location = f"@{self.fname}"
        self.env.add(self.fname, node)

        # Append the function definition instruction.
        func_type = node.declarator.check_type
        ret_type = func_type.ret_type
        inst = [f"define_{ret_type.opname}", node.gen_location]
        if func_type.param_types[0] is not VoidType:
            for param_type, param_name in zip(func_type.param_types,
                                              func_type.param_names):
                inst.append((param_type.opname, f"%{param_name}"))
        self.code.append(tuple(inst))

        # Make sure to skip the visit to the Decl node jump directly to the
        # FuncDecl node.
        self.env.push_scope(node)
        self.visit(node.declarator.type)
        self.visit(node.comp_stm)

        # Compute return instructions for function exit
        retval_node = self.env.lookup("retval")
        optype = retval_node.check_type.opname
        if optype == "void":
            ret_insts = [("return_void",)]
        else:
            target = self.new_temp()
            ret_insts = [
                (f"load_{optype}", retval_node.gen_location, target),
                (f"return_{optype}", target),
            ]
        retaddr_node = self.env.lookup("retaddr")
        self.code.append(("jump", retaddr_node.gen_location))
        retaddr_label = self.get_label(retaddr_node.gen_location)
        self.code.append((retaddr_label,))
        self.code.extend(ret_insts)

        self.env.pop_scope()

    def visit_FuncDecl(self, node):
        """Visit function declaration node.

        This is the node where we allocate temporaries for the function
        arguments and store the return temporary along with it's type in the
        symbol table for future reference.
        """
        # Allocate new temps for each parameter.
        if node.params is not None:
            for param_decl in node.params.params:
                self.new_temp(param_decl.name.name)

        # Allocate a new temp for the function's return value.
        ret_target = self.new_temp(".ret")
        node.type.gen_location = ret_target
        self.env.add("retval", node.type)

        # If the return type isn't void, append an alloc instruction.
        if node.type.check_type is not VoidType:
            inst = (f"alloc_{node.type.check_type.opname}", ret_target)
            self.code.append(inst)

        if node.params is not None:
            self.visit(node.params)

        # Create a new temporary for the function's return address.
        ret_addr_target = self.new_temp("exit")
        node.gen_location = ret_addr_target
        self.env.add("retaddr", node)

    def visit_ParamList(self, node):
        """Visit parameter list node.

        Ideally, we would like to just visit the Decl nodes from this one. But
        the fact that we need to generate instructions that store the argument
        targets into the allocated targets for the function parameters makes it
        extremely hard to code a generic Decl behavior that would work for this
        case. Therefore, we opted to create the alloc and store instructions
        for the parameter list in this node.
        """
        # Allocate temps for the local variables which will store the passed
        # arguments.
        param_targs = []
        param_types = []
        for param in node.params:
            target = self.new_temp(f"{param.name.name}.addr")

            # Keep track of target and opname for each parameter
            param_targs.append(target)
            param_types.append(param.check_type.opname)

            # Add parameter to symbol table.
            param.gen_location = target
            self.env.add(param.name.name, param)

            inst = (f"alloc_{param.check_type.opname}", target)
            self.code.append(inst)

        # Store each of the arguments in their respective local variables.
        for param_type, param_addr in zip(param_types, param_targs):
            param_name = param_addr.split(".")[0]
            inst = (f"store_{param_type}", param_name, param_addr)
            self.code.append(inst)

    def visit_Return(self, node):
        """Visit the return node."""
        if node.expr is not None:
            self.visit(node.expr)

            retval_node = self.env.lookup("retval")
            assert retval_node is not None

            optype = retval_node.check_type.opname
            inst = (f"store_{optype}",
                    node.expr.gen_location,
                    retval_node.gen_location)
            self.code.append(inst)
        retaddr_node = self.env.lookup("retaddr")
        self.code.append(("jump", retaddr_node.gen_location))

    def visit_ExprList(self, node):
        """Visit expression list node."""
        gen_locs = []
        for expr in node.exprs:
            self.visit(expr)
            gen_locs.append((expr.check_type.opname, expr.gen_location))
        node.gen_location = gen_locs

    def visit_FuncCall(self, node):
        """Visit function call node."""
        # Set gen_addr to true since we wan't the address of the function
        # to make the call instruction.
        node.pexpr.gen_addr = True
        self.visit(node.pexpr)
        if node.arg_expr is not None:
            self.visit(node.arg_expr)

            # Make sure that gen_locations is always a list
            gen_locs = node.arg_expr.gen_location
            if not isinstance(node.arg_expr, ast.ExprList):
                gen_locs = [(node.arg_expr.check_type.opname,
                             node.arg_expr.gen_location)]

            for param_type, param_target in gen_locs:
                inst = (f"param_{param_type}", param_target)
                self.code.append(inst)

        # Append the call instruction
        ret_target = self.new_temp()
        ret_type = node.pexpr.check_type.ret_type
        inst = (f"call_{ret_type.opname}", node.pexpr.gen_location, ret_target)
        self.code.append(inst)

        node.gen_location = ret_target

    def visit_Assignment(self, node):
        """Visit assignment node."""
        if node.op == "=":
            # We can't visit the unary_expr node since that would create a load
            # instruction for the identifier and a new temporary. Instead, we
            # want to access the memory address for the identifier directly.
            node.unary_expr.gen_addr = True
            self.visit(node.unary_expr)
            self.visit(node.assign_expr)

            if isinstance(node.unary_expr.check_type, PtrType):
                # If unary expr type is a pointer, we need to store the address
                # of source, instead of its value.
                opcode = f"get_{node.unary_expr.check_type.opname}"
            else:
                # If the lvalue is an array ref, we need to store a pointer to
                # match the return target from elem_type.
                opcode = f"store_{node.unary_expr.check_type.opname}"
                if isinstance(node.unary_expr, ast.ArrayRef):
                    opcode += "_*"

            inst = (opcode,
                    node.assign_expr.gen_location,
                    node.unary_expr.gen_location)
            self.code.append(inst)
            node.gen_location = node.assign_expr.gen_location
        else:
            # We can decompose the different assignment operators as binary
            # ops followed by assignment ops, such as:
            # a += b ----> a = a + b
            # The binary op is the first character of the assignment op.
            binop = node.op[0]

            # Since the unary expression will appear on both the left and right
            # side of the expression, we need to make a deep copy and assign
            # gen_addr to False, in order for us to use the address on the left
            # side and the value on the right side.
            right_unary_expr = copy.deepcopy(node.unary_expr)
            right_unary_expr.gen_addr = False

            binop_node = ast.BinaryOp(binop,
                                      node.assign_expr,
                                      right_unary_expr)
            assign_node = ast.Assignment("=", node.unary_expr, binop_node)
            self.visit(assign_node)
            node.gen_location = assign_node.gen_location

    def visit_Assert(self, node):
        """Visit assert node."""
        self.visit(node.expr)

        # Store the assertion error string as a global
        assert_msg_str = f"assertion_fail on {node.expr.coord.line}:" \
                         f"{node.expr.coord.column}"
        assert_msg_target = self.store_global_const("string", assert_msg_str)

        true_target = self.new_temp("assert.true")
        false_target = self.new_temp("assert.false")
        inst = ("cbranch", node.expr.gen_location, true_target, false_target)
        self.code.append(inst)

        # Append the false block with a jump to exit on the end.
        self.code.append((self.get_label(false_target),))
        self.code.append(("print_string", assert_msg_target))
        retaddr_node = self.env.lookup("retaddr")
        self.code.append(("jump", retaddr_node.gen_location))

        # Append the true block.
        self.code.append((self.get_label(true_target),))

    def visit_Print(self, node):
        """Visit the print statement node."""
        if node.expr is not None:
            if isinstance(node.expr, ast.ExprList):
                for expr in node.expr.exprs:
                    self.visit(expr)
                    inst = (f"print_{expr.check_type.opname}",
                            expr.gen_location)
                    self.code.append(inst)
            else:
                self.visit(node.expr)
                inst = (f"print_{node.expr.check_type.opname}",
                        node.expr.gen_location)
                self.code.append(inst)
        else:
            # If print is called with no expression, just print a newline.
            print_target = self.store_global_const(
                opname="string",
                value="\n"
            )
            inst = (f"print_string", print_target)
            self.code.append(inst)

    def visit_UnaryOp(self, node):
        """Visit the unary operation node."""
        if node.op == "++" or node.op == "p++" or \
           node.op == "--" or node.op == "p--":
            # Get the load temp and address temp of the expression
            self.visit(node.expr)
            load_gen_loc = node.expr.gen_location
            node.expr.gen_addr = True
            self.visit(node.expr)
            addr_gen_loc = node.expr.gen_location

            typename = node.expr.check_type.opname

            # Generate the literal instruction
            lit_const = 1
            if typename == "float":
                lit_const = 1.0
            lit_target = self.new_temp()
            lit_inst = (f"literal_{typename}", lit_const, lit_target)
            self.code.append(lit_inst)

            # Generate the binary op instruction.
            binop_target = self.new_temp()
            binop_name = "sub" if node.op[-1] == "-" else "add"
            binop_inst = (f"{binop_name}_{typename}",
                          load_gen_loc,
                          lit_target,
                          binop_target)
            self.code.append(binop_inst)

            # Store the newly computed expression
            store_inst = (f"store_{typename}", binop_target, addr_gen_loc)
            self.code.append(store_inst)

            if node.op[0] == "p":
                node.gen_location = load_gen_loc
            else:
                node.gen_location = binop_target
        elif node.op == "-":
            self.visit(node.expr)
            typename = node.expr.check_type.opname

            # Generate the literal instruction
            lit_const = 0
            if typename == "float":
                lit_const = 0.0
            lit_target = self.new_temp()
            lit_inst = (f"literal_{typename}", lit_const, lit_target)
            self.code.append(lit_inst)

            # Generate the subtraction instruction to negate the value of expr.
            sub_target = self.new_temp()
            sub_inst = (f"sub_{typename}",
                        lit_target,
                        node.expr.gen_location,
                        sub_target)
            self.code.append(sub_inst)

            node.gen_location = sub_target
        elif node.op == "+":
            self.visit(node.expr)
            node.gen_location = node.expr.gen_location
        elif node.op == "!":
            self.visit(node.expr)
            target = self.new_temp()
            inst = (f"not_{node.expr.check_type.opname}",
                    node.expr.gen_location,
                    target)
            self.code.append(inst)
            node.gen_location = target
        elif node.op == "&":
            node.expr.gen_addr = True
            self.visit(node.expr)
            node.gen_location = node.expr.gen_location
        elif node.op == "*":
            pass

    def visit_Cast(self, node):
        """Visit the cast node.

        Currently we don't have a way to handle casts to char or void.
        """
        self.visit(node.cast_expr)

        assert len(node.type.names) == 1
        target = self.new_temp()
        typename = node.type.names[0]
        if typename == "float":
            opcode = "sitofp"
        else:
            opcode = "fptosi"
        inst = (opcode, node.cast_expr.gen_location, target)
        self.code.append(inst)
        node.gen_location = target

    def visit_For(self, node):
        """Visit for statement node."""
        # Push a new scope for declarations made within the for statement.
        self.env.push_scope(node)

        # Allocate temporaries for the labels we need to define the for.
        cond_target = self.new_temp("for.cond")
        stm_target = self.new_temp("for.body")
        end_target = self.new_temp("for.end")

        # Add the label to exit the for statement to the break stack.
        self.break_stack.append(end_target)

        if node.expr1 is not None:
            self.visit(node.expr1)
        self.code.append(("jump", cond_target))
        self.code.append((self.get_label(cond_target),))
        if node.expr2 is not None:
            self.visit(node.expr2)
            inst = ("cbranch",
                    node.expr2.gen_location,
                    stm_target,
                    end_target)
            self.code.append(inst)
        self.code.append((self.get_label(stm_target),))
        self.visit(node.statement)
        if node.expr3 is not None:
            self.visit(node.expr3)
        self.code.append(("jump", cond_target))
        self.code.append((self.get_label(end_target),))

        # Pop the exit address from the break stack
        self.break_stack.pop()
        # Pop the for scope from the environment.
        self.env.pop_scope()

    def visit_While(self, node):
        """Visit the while statement node."""
        cond_target = self.new_temp("while.cond")
        stm_target = self.new_temp("while.body")
        end_target = self.new_temp("while.end")

        # Add the label to exit the while statement to the break stack.
        self.break_stack.append(end_target)

        self.code.append(("jump", cond_target))
        self.code.append((self.get_label(cond_target),))
        self.visit(node.expr)
        inst = ("cbranch",
                node.expr.gen_location,
                stm_target,
                end_target)
        self.code.append(inst)
        self.code.append((self.get_label(stm_target),))
        self.visit(node.statement)
        self.code.append(("jump", cond_target))
        self.code.append((self.get_label(end_target),))
        self.break_stack.pop()

    def visit_Break(self, node):
        # pylint: disable=unused-argument
        """Visit the break statement.

        A break is simply a jump to exit the enclosing for or while statement.
        """
        assert len(self.break_stack) > 0
        self.code.append(("jump", self.break_stack[-1]))

    def visit_Read(self, node):
        """Visit the read statement node."""
        if isinstance(node.arg_expr, ast.ExprList):
            for expr in node.arg_expr.exprs:
                expr.gen_addr = True
                self.visit(expr)
                opcode = f"read_{expr.check_type.opname}"
                if isinstance(expr, ast.ArrayRef):
                    opcode += "_*"
                read_inst = (opcode, expr.gen_location)
                self.code.append(read_inst)
        else:
            node.arg_expr.gen_addr = True
            self.visit(node.arg_expr)
            opcode = f"read_{node.arg_expr.check_type.opname}"
            if isinstance(node.arg_expr, ast.ArrayRef):
                opcode += "_*"
            read_inst = (opcode, node.arg_expr.gen_location)
            self.code.append(read_inst)
            opcode = f"store_{node.arg_expr.check_type.opname}"

    def visit_If(self, node):
        """Visit the if and else statement node."""
        self.visit(node.expr)
        then_target = self.new_temp("if.then")
        end_target = self.new_temp("if.end")
        else_target = None
        if node.else_stm is not None:
            else_target = self.new_temp("if.else")
            inst = \
                ("cbranch", node.expr.gen_location, then_target, else_target)
        else:
            inst = ("cbranch", node.expr.gen_location, then_target, end_target)
        self.code.append(inst)

        # Append the if.then block instructions.
        self.code.append((self.get_label(then_target),))
        self.visit(node.if_stm)
        self.code.append(("jump", end_target))

        # Append the if.else block instructions.
        if node.else_stm is not None:
            self.code.append((self.get_label(else_target),))
            self.visit(node.else_stm)
            self.code.append(("jump", end_target))

        self.code.append((self.get_label(end_target),))

    def visit_ArrayRef(self, node):
        """Visit array reference node."""
        # We always know the size of the array a priori, so the first step
        # is to genereate a binary op node that computes that offset from the
        # array's root address to the element we want to access.
        if isinstance(node.pexpr, ast.ArrayRef):
            node.pexpr.pexpr.gen_addr = True
            self.visit(node.pexpr.pexpr)

            # Load the number of columns of the matrix as a literal.
            lit_target = self.new_temp()
            lit_inst = ("literal_int",
                        node.pexpr.check_type.size,
                        lit_target)
            self.code.append(lit_inst)

            # Compute the row offset by multiplying the row expression by the
            # number of columns.
            self.visit(node.pexpr.expr)
            mul_target = self.new_temp()
            mul_inst = ("mul_int",
                        node.pexpr.expr.gen_location,
                        lit_target,
                        mul_target)
            self.code.append(mul_inst)

            # Compute the column offset
            self.visit(node.expr)

            # Compute the total offset for the target element.
            add_target = self.new_temp()
            add_inst = ("add_int",
                        mul_target,
                        node.expr.gen_location,
                        add_target)
            self.code.append(add_inst)
            elem_target = self.new_temp()
            elem_inst = (f"elem_{node.check_type.opname}",
                         node.pexpr.pexpr.gen_location,
                         add_target,
                         elem_target)
            self.code.append(elem_inst)
        else:
            self.visit(node.expr)
            node.pexpr.gen_addr = True
            self.visit(node.pexpr)
            elem_target = self.new_temp()
            elem_inst = (f"elem_{node.check_type.opname}",
                         node.pexpr.gen_location,
                         node.expr.gen_location,
                         elem_target)
            self.code.append(elem_inst)

        # Load the element in the computed address. If gen_addr is False,
        # we just return the address of the element we're accessing.
        if not node.gen_addr:
            load_target = self.new_temp()
            load_inst = (f"load_{node.check_type.opname}_*",
                         elem_target,
                         load_target)
            self.code.append(load_inst)
            node.gen_location = load_target
        else:
            node.gen_location = elem_target


def main():
    """Parse input stream and check the program for the given source code."""
    parser = argparse.ArgumentParser(description="Run the uC program checker.")
    parser.add_argument(
        "filename", type=str, nargs="?", default=None,
        help="The filename with the uC code that should be checked. In case "
             "no argument filename is provided, stdin is used by default.")
    args = parser.parse_args()

    if args.filename:
        file = open(args.filename, 'r')
    else:
        file = sys.stdin

    ucparser = UCParser()
    ast_root = ucparser.parse(input_code=file.read())
    if ucparser.nerrors == 0:
        with subscribe_errors(lambda msg: sys.stdout.write(msg + '\n')):
            try:
                prog_check = ProgramCheckVisitor()
                prog_check.visit(ast_root)
                if num_errors_reported() == 0:
                    code_gen = UCCodeGen()
                    code_gen.visit(ast_root)
                    print(code_gen)
            except SemanticError:
                pass


if __name__ == "__main__":
    main()
