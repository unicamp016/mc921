"""LLVM IR code generator."""
import argparse
import sys
from ctypes import CFUNCTYPE, c_int32, c_voidp

import llvmlite.ir as ir
import llvmlite.binding as binding

from dataflow import optimize_code
from exprcheck import ProgramCheckVisitor
from exprcode import UCCodeGen
from exprerror import num_errors_reported, subscribe_errors, SemanticError
from parser import UCParser

bit_t = ir.IntType(1)
char_t = ir.IntType(8)
int32_t = ir.IntType(32)
int64_t = ir.IntType(64)
float_t = ir.FloatType()
void_t = ir.VoidType()
char_ptr_t = char_t.as_pointer()

_CMP_MAP = {
    "lt": "<",
    "le": "<=",
    "eq": "==",
    "ne": "!=",
    "ge": ">=",
    "gt": ">",
}

_BUILDER_FUNC_MAP = {
    "add_int": "add",
    "add_float": "fadd",
    "sub_int": "sub",
    "sub_float": "fsub",
    "mul_int": "mul",
    "mul_float": "fmul",
    "div_int": "sdiv",
    "div_float": "fdiv",
    "mod_int": "srem",
    "mod_float": "frem",
    "and": "and_",
    "or": "or_",
}


class LLVMCodeGen:
    """Class to generate LLVM IR code.

    Attributes:
        ucir_code: The original uC IR code used to translate to LLVM IR.
        module: The ir.Module object used to build and group all functions
            together.
        builder: LLVM IR helper object to build the instructions.
        memcpy: LLVM function used to copy arrays of data. The function accepts
            the following arguments:
            * src: char* - Pointer to the beginning of the source array.
            * dst: char* - Pointer to the beginning of the destination array.
            * len: int64 - The number of bytes to copy from src to dst.
        printf: LLVM function to print arguments to stdout. The arguments are:
            * fmt: char* - Null-terminated string with the format used to print
                the values.
            * args: void* - Variable number of arguments with types that must
                match the ones used in the format string.
        scanf: LLVM function to read data from stdin. The arguments are:
            * fmt: char* - Null-terminated string with the format used to scan
                the values from stdin.
            * args: void* - Variable number of arguments with types that must
                match the ones used in the format string.
        symbols: Dictionary of previously declared symbols. This is used to
            keep track of the instruction results and propagate them through
            the CFG.
        fn_args: List of function arguments used in call instructions. Values
            are pushed to the list on every param instruction and are popped
            after the function is called.
    """

    def __init__(self, module_name, ucir_code):
        """Initialize code generator with module_name and uC IR code."""
        # Code generation initializations
        binding.initialize()
        binding.initialize_native_target()
        binding.initialize_native_asmprinter()
        self.ucir_code = ucir_code
        self.module = ir.Module(module_name)
        self.builder = ir.IRBuilder()
        self.memcpy = self._declare_memcpy()
        self.printf = self._declare_printf()
        self.scanf = self._declare_scanf()
        self._declare_module_funcs()  # Declare all functions before parsing.
        self.symbols = {}
        self.fn_args = []
        self.engine = self._create_execution_engine()
        self._method_cache = {}

    def __str__(self):
        """Return a string with the LLVM IR."""
        return str(self.module)

    def generate_ir(self, debug=False):
        """Iterate over the uC IR instructions and generating the LLVM code."""
        for inst in self.ucir_code:
            builder = self._get_builder(inst)
            builder(inst)
        if debug:
            print("LLVM code generated from direct translation from uC IR:\n")
            print(self)

    def compile_ir(self, llvm_opts=None, debug=False):
        """Compile the IR with the host's engine."""
        module_ref = binding.parse_assembly(str(self.module))
        module_ref.verify()
        self.engine.add_module(module_ref)
        self.engine.finalize_object()
        self.engine.run_static_constructors()
        if llvm_opts is not None:
            pm = binding.create_module_pass_manager()
            for llvm_opt in llvm_opts:
                if llvm_opt == "dce":
                    pm.add_dead_code_elimination_pass()
                elif llvm_opt == "cfg":
                    pm.add_cfg_simplification_pass()
                elif llvm_opt == "ctm":
                    pm.add_sccp_pass()
                    pm.add_constant_merge_pass()
                    pm.add_instruction_combining_pass()
            pm.run(module_ref)
            if debug:
                print("\n\nLLVM code after performing optimization passes:\n")
                print(module_ref)
        return self.module

    def run(self):
        """Run the module's main function."""
        func_ptr = self.engine.get_function_address("main")
        if self.module.get_global("main").ftype.return_type is void_t:
            main_func = CFUNCTYPE(c_voidp)(func_ptr)
            main_func()
            ret = 0
        else:
            main_func = CFUNCTYPE(c_int32)(func_ptr)
            ret = main_func()
        sys.exit(ret)

    def _declare_memcpy(self):
        memcpy = self.module.declare_intrinsic(
            "llvm.memcpy",
            [char_ptr_t, char_ptr_t, int64_t]
        )
        return memcpy

    def _declare_printf(self):
        printf_func_t = ir.FunctionType(int32_t, [char_ptr_t], var_arg=True)
        printf = ir.Function(self.module, printf_func_t, name="printf")
        return printf

    def _declare_scanf(self):
        scanf_func_t = ir.FunctionType(int32_t, [char_ptr_t], var_arg=True)
        scanf = ir.Function(self.module, scanf_func_t, name="scanf")
        return scanf

    def _declare_module_funcs(self):
        for inst in self.ucir_code:
            opname = inst[0].split("_")[0]
            if opname == "define":
                func_name = inst[1].partition("@")[-1]
                ret_ucir_t = inst[0].partition("_")[-1]
                args_ucir_t = [arg[0] for arg in inst[2:]]
                func_llvm_t = ir.FunctionType(
                    return_type=self._cast_to_llvm(ret_ucir_t),
                    args=[self._cast_to_llvm(arg_t) for arg_t in args_ucir_t]
                )
                ir.Function(self.module, func_llvm_t, name=func_name)

    def _create_execution_engine(self):
        """Create an execution engine for JIT code generation in the host CPU.

        The engine is reusable for an arbitrary number of modules.
        """
        # Create a target machine representing the host.
        target = binding.Target.from_default_triple()
        target_machine = target.create_target_machine()
        # Add an execution engine with an empty backing module.
        backing_mod = binding.parse_assembly("")
        return binding.create_mcjit_compiler(backing_mod, target_machine)

    def _get_builder(self, inst):
        """Return the method that builds the LLVM IR for the instruction."""
        if inst[0].endswith(":"):
            return self._build_label
        inst_name = inst[0].partition("_")[0]
        builder = self._method_cache.get(inst_name, None)
        if builder is None:
            method_name = f"_build_{inst_name}"
            builder = getattr(self, method_name)
            self._method_cache[inst_name] = builder
        return builder

    def _cast_to_llvm(self, ucir_type):
        """Return the llvm type for the given ucir type."""
        if ucir_type == "int":
            return int32_t
        elif ucir_type == "float":
            return float_t
        elif ucir_type == "char":
            return char_t
        elif ucir_type == "void":
            return void_t
        elif "*" in ucir_type:
            # Return the pointer's basic type.
            return self._cast_to_llvm(ucir_type.partition("_")[0])
        elif "_" in ucir_type:
            base_type = ucir_type.split("_")[0]
            dims = list(map(int, ucir_type.split("_")[1:]))
            llvm_type = self._cast_to_llvm(base_type)
            for dim in reversed(dims):
                llvm_type = ir.ArrayType(llvm_type, dim)
            return llvm_type
        else:
            raise ValueError(f"Invalid uC IR type {ucir_type}")

    def _partition_inst(self, inst):
        """Return the LLVM type and target name for the uC IR instruction."""
        ucir_t = inst[0].partition("_")[-1]
        llvm_t = self._cast_to_llvm(ucir_t)
        target_name = inst[-1][1:]
        return llvm_t, target_name

    def _get_llvm_const(self, llvm_type, value):
        """Return an ir.Constant object for the ir.Type and Python value."""
        if isinstance(llvm_type, ir.ArrayType):
            # For strings, we return a bytearray.
            if llvm_type.element is char_t:
                return ir.Constant(llvm_type, bytearray(str(value), "utf8"))
            # Otherwise we recurse until the base type.
            llvm_elems = []
            for v in value:
                llvm_elems.append(self._get_llvm_const(llvm_type.element, v))
            return ir.Constant.literal_array(llvm_elems)
        elif llvm_type is char_t:
            return ir.Constant(llvm_type, ord(value))
        elif llvm_type is int32_t or llvm_type is float_t:
            return ir.Constant(llvm_type, value)
        else:
            raise ValueError(f"Can't build const for unknown type {llvm_type}")

    def _get_byte_size(self, llvm_type):
        """Return the size in bytes of the LLVM type."""
        if isinstance(llvm_type, ir.IntType):
            return llvm_type.width // 8
        elif llvm_type is float_t:
            return 4
        elif isinstance(llvm_type, ir.ArrayType):
            return llvm_type.count * self._get_byte_size(llvm_type.element)
        else:
            raise ValueError(f"Can't compute size for type {llvm_type}.")

    def _reset_symbols(self):
        """Reset the symbols dictionary only keeping global values."""
        self.symbols = {}
        for gv in self.module.global_values:
            self.symbols[gv.name] = gv

    def _build_define(self, inst):
        self._reset_symbols()
        func_name = inst[1].partition("@")[-1]
        if func_name not in self.symbols:
            ret_ucir_t = inst[0].partition("_")[-1]
            args_ucir_t = [arg[0] for arg in inst[2:]]
            func_llvm_t = ir.FunctionType(
                return_type=self._cast_to_llvm(ret_ucir_t),
                args=[self._cast_to_llvm(arg_t) for arg_t in args_ucir_t]
            )
            func = ir.Function(self.module, func_llvm_t, name=func_name)
        else:
            func = self.module.get_global(func_name)
        entry_block = func.append_basic_block(name="entry")
        self.builder.position_at_start(entry_block)

        arg_names = [arg[1][1:] for arg in inst[2:]]
        for idx, arg_name in enumerate(arg_names):
            self.symbols[arg_name] = func.args[idx]

    def _build_global(self, inst):
        ucir_t = inst[0].partition("_")[-1]
        global_name = inst[1][1:]
        if ucir_t == "string":
            global_t = ir.ArrayType(char_t, len(inst[2]) + 1)
            global_var = ir.GlobalVariable(self.module, global_t, global_name)
            global_var.global_constant = True
            string_val = inst[2] + '\x00'
            global_var.initializer = self._get_llvm_const(global_t, string_val)
        else:
            global_t = self._cast_to_llvm(ucir_t)
            global_var = ir.GlobalVariable(self.module, global_t, global_name)
            if len(inst) > 2:
                global_var.initializer = \
                    self._get_llvm_const(global_t, inst[2])
        self.symbols[global_name] = global_var

    def _build_label(self, inst):
        block_name = inst[0][:-1]
        block = self.symbols.get(block_name, None)
        if block is None:
            block = self.builder.append_basic_block(block_name)
            self.symbols[block_name] = block
        self.builder.position_at_start(block)

    def _build_jump(self, inst):
        if self.builder.block.is_terminated:
            return
        jump_tgt = inst[-1][1:]
        jump_block = self.symbols.get(jump_tgt, None)
        if jump_block is None:
            jump_block = self.builder.append_basic_block(jump_tgt)
            self.symbols[jump_tgt] = jump_block
        self.builder.branch(jump_block)

    def _build_cbranch(self, inst):
        if self.builder.block.is_terminated:
            return
        pred_val = self.symbols[inst[1][1:]]
        true_label = inst[2][1:]
        false_label = inst[3][1:]
        true_block = self.symbols.get(true_label, None)
        if true_block is None:
            true_block = self.builder.append_basic_block(true_label)
            self.symbols[true_label] = true_block
        false_block = self.symbols.get(false_label, None)
        if false_block is None:
            false_block = self.builder.append_basic_block(false_label)
            self.symbols[false_label] = false_block
        self.builder.cbranch(pred_val, true_block, false_block)

    def _build_return(self, inst):
        if len(inst) == 1:
            assert inst[0] == "return_void"
            self.builder.ret_void()
        else:
            _, ret_tgt = self._partition_inst(inst)
            ret_val = self.symbols[ret_tgt]
            self.builder.ret(ret_val)

    def _build_literal(self, inst):
        literal_t, literal_tgt = self._partition_inst(inst)
        llvm_const = self._get_llvm_const(literal_t, inst[1])

        # If we've seen the target before, this means it's a variable and we
        # should perform a store operation. Otherwise, we just update the
        # symbols dictionary for a future store instruction to use the const.
        if literal_tgt in self.symbols:
            self.builder.store(llvm_const, self.symbols[literal_tgt])
        else:
            self.symbols[literal_tgt] = llvm_const

    def _build_alloc(self, inst):
        alloc_t, alloc_tgt = self._partition_inst(inst)
        alloc_inst = self.builder.alloca(alloc_t, name=alloc_tgt)
        self.symbols[alloc_tgt] = alloc_inst

    def _build_store(self, inst):
        store_t, store_tgt = self._partition_inst(inst)
        store_src = inst[1][1:]
        if store_src in self.symbols:
            constant = self.symbols[store_src]
        else:
            constant = ir.Constant(store_t, inst[1])
        if isinstance(store_t, ir.ArrayType):
            mem_src = self.builder.bitcast(constant, char_ptr_t)
            mem_dst = self.builder.bitcast(self.symbols[store_tgt], char_ptr_t)
            byte_size = self._get_byte_size(store_t)
            self.builder.call(
                self.memcpy,
                [mem_dst, mem_src, int64_t(byte_size), bit_t(False)]
            )
        else:
            self.builder.store(constant, self.symbols[store_tgt])

    def _build_load(self, inst):
        load_src, load_tgt = inst[1][1:], inst[2][1:]
        load_val = self.symbols[load_src]
        load_inst = self.builder.load(load_val, name=load_tgt)
        self.symbols[load_tgt] = load_inst

    def _partition_binop(self, inst):
        lhs_val = self.symbols[inst[1][1:]]
        rhs_val = self.symbols[inst[2][1:]]
        bin_tgt = inst[3][1:]
        return lhs_val, rhs_val, bin_tgt

    def _build_cmp(self, inst):
        ucir_opname, ucir_type = inst[0].split("_")
        lhs_val, rhs_val, cmp_tgt = self._partition_binop(inst)
        if ucir_type in ("int", "char"):
            llvm_inst = self.builder.icmp_signed
        elif ucir_type == "float":
            llvm_inst = self.builder.fcmp_ordered
        else:
            raise ValueError(f"Unrecognized type {ucir_type} for cmp inst.")
        cmp_val = llvm_inst(
            cmpop=_CMP_MAP[ucir_opname],
            lhs=lhs_val,
            rhs=rhs_val,
            name=cmp_tgt
        )
        self.symbols[cmp_tgt] = cmp_val

    _build_eq = _build_cmp
    _build_lt = _build_cmp
    _build_le = _build_cmp
    _build_ne = _build_cmp
    _build_ge = _build_cmp
    _build_gt = _build_cmp

    def _build_binop(self, inst):
        ucir_opname = inst[0]
        lhs_val, rhs_val, binop_tgt = self._partition_binop(inst)
        llvm_opname = getattr(self.builder, _BUILDER_FUNC_MAP[ucir_opname])
        binop_inst = llvm_opname(lhs=lhs_val, rhs=rhs_val, name=binop_tgt)
        self.symbols[binop_tgt] = binop_inst

    _build_add = _build_binop
    _build_sub = _build_binop
    _build_mul = _build_binop
    _build_div = _build_binop
    _build_mod = _build_binop

    def _build_logop(self, inst):
        ucir_opname = inst[0].partition("_")[0]
        lhs_val, rhs_val, logop_tgt = self._partition_binop(inst)
        llvm_opname = getattr(self.builder, _BUILDER_FUNC_MAP[ucir_opname])
        logop_inst = llvm_opname(lhs=lhs_val, rhs=rhs_val, name=logop_tgt)
        self.symbols[logop_tgt] = logop_inst

    _build_or = _build_logop
    _build_and = _build_logop

    def _build_not(self, inst):
        not_tgt = inst[2][1:]
        not_inst = self.builder.not_(self.symbols[inst[1][1:]], not_tgt)
        self.symbols[not_tgt] = not_inst

    def _build_cast(self, inst):
        cast_tgt = inst[2][1:]
        cast_typ = float_t if inst[0] == "sitofp" else int32_t
        llvm_opname = getattr(self.builder, inst[0])
        cast_inst = llvm_opname(self.symbols[inst[1][1:]], cast_typ, cast_tgt)
        self.symbols[cast_tgt] = cast_inst

    _build_sitofp = _build_cast
    _build_fptosi = _build_cast

    def _build_param(self, inst):
        self.fn_args.append(self.symbols[inst[1][1:]])

    def _build_call(self, inst):
        llvm_fn = self.module.get_global(inst[1][1:])
        if llvm_fn.type is void_t:
            self.builder.call(llvm_fn, self.fn_args)
        else:
            call_tgt = inst[2][1:]
            call_inst = self.builder.call(llvm_fn, self.fn_args, call_tgt)
            self.symbols[call_tgt] = call_inst
        self.fn_args = []

    def _build_io(self, inst):
        opname = inst[0].partition("_")[0]
        io_t = inst[0].partition("_")[-1]
        io_val = self.symbols[inst[1][1:]]

        if "int" in io_t:
            io_fmt = "%d\x00"
        elif "char" in io_t:
            io_fmt = "%c\x00"
        elif "float" in io_t:
            io_fmt = "%.2f\x00"
        elif "string" in io_t:
            io_fmt = "%s\x00"
        else:
            raise ValueError(f"Unknown print format for type {io_t}")

        # Add io_fmt as a global string
        io_fmt_t = ir.ArrayType(char_t, len(io_fmt))
        io_fmt_var = ir.GlobalVariable(
            self.module, io_fmt_t, self.module.get_unique_name(".fmt"))
        io_fmt_var.global_constant = True
        io_fmt_var.initializer = \
            self._get_llvm_const(io_fmt_t, io_fmt)
        io_fmt_bitc = self.builder.bitcast(io_fmt_var, char_ptr_t)
        # Switch between printf and scanf
        io_func = self.printf if opname == "print" else self.scanf
        self.builder.call(io_func, [io_fmt_bitc, io_val])

    _build_print = _build_io
    _build_read = _build_io

    def _build_elem(self, inst):
        arr_val = self.symbols[inst[1][1:]]
        idx_val = self.symbols[inst[2][1:]]
        elem_tgt = inst[3][1:]
        assert isinstance(arr_val.type.pointee, ir.ArrayType)

        if isinstance(arr_val.type.pointee.element, ir.ArrayType):
            inner_arr_size = arr_val.type.pointee.element.count
            div_inst = self.builder.sdiv(
                lhs=idx_val,
                rhs=int32_t(inner_arr_size),
                name=self.module.get_unique_name(".elem.div")
            )
            rem_inst = self.builder.srem(
                lhs=idx_val,
                rhs=int32_t(inner_arr_size),
                name=self.module.get_unique_name(".elem.rem")
            )
            elem_inst = self.builder.gep(
                arr_val,
                [int32_t(0), div_inst, rem_inst],
                name=elem_tgt
            )
        else:
            elem_inst = \
                self.builder.gep(arr_val, [int32_t(0), idx_val], name=elem_tgt)
        self.symbols[elem_tgt] = elem_inst


def main():
    """Parse input stream and run the program."""
    parser = argparse.ArgumentParser(description="Run the uC interpreter.")
    parser.add_argument(
        "filename", type=str, nargs="?", default=None,
        help="The filename with the uC code that should be checked. In case "
             "no argument filename is provided, stdin is used by default.")
    parser.add_argument(
        "-o", "--opt", action="store_true",
        help="Optimize the uC IR before translating to LLVM.")
    parser.add_argument(
        "-p", "--llvm-opts", nargs='*', choices=['ctm', 'dce', 'cfg'],
        help="Specify which llvm pass optimizations is enabled")
    parser.add_argument(
        "-g", "--debug", action="store_true",
        help="When run in debugging mode, the code outputs the generated LLVM "
             "IR")
    args = parser.parse_args()

    if args.filename:
        module_name = args.filename
        file = open(args.filename, 'r')
    else:
        module_name = "stdin.uc"
        file = sys.stdin

    ucparser = UCParser()
    ast_root = ucparser.parse(input_code=file.read())
    if ucparser.nerrors == 0:
        with subscribe_errors(lambda msg: sys.stdout.write(msg + '\n')):
            try:
                prog_check = ProgramCheckVisitor()
                prog_check.visit(ast_root)
                if num_errors_reported() == 0:
                    code_gen = UCCodeGen()
                    code_gen.visit(ast_root)
                    code = code_gen.code
                    if args.opt:
                        code = optimize_code(code, llvm=True)
                    llvm_code_gen = LLVMCodeGen(module_name, code)
                    llvm_code_gen.generate_ir(args.debug)
                    llvm_code_gen.compile_ir(args.llvm_opts, args.debug)
                    llvm_code_gen.run()
            except SemanticError:
                pass


if __name__ == "__main__":
    main()
