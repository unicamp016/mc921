int main() {
  int x, y = 3;
  float z = 4.5;
  x = y + 5;
  z = (float)x;
  y = (int)z;
  assert z == 8.0 && x == 8;
  return 0;
}
