int main () {
  int a[3][2] = {{1, 2}, {10, 4}, {5, 6}};
  int b = a[0][1];
  int c[5];
  int i = 0;

  c[2] = a[i + 2][i + 1];
  i++;
  a[i - 1][i - 1] = 150;

  assert c[2] == 6 && b == 2 && a[0][0] == 150;
  return 0;
}
